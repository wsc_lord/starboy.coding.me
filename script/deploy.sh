#! /bin/sh

# 参考文章
# https://blog.csdn.net/qq_36881106/article/details/82623271
# http://www.west.cn/docs/51562.html

# 拷贝脚本的时候运行的时候如果报错，应该是文件格式转换的问题
# bash: ./a.sh: /bin/bash^M: bad interpreter: No such file or directory
# yum -y install dos2unix
# dos2unix deploy.sh

# 第一次运行的时候获取一下超级管理员权限
# chmod +x deploy.sh

# springboot打包后的jar所在目录
BASE_HOME=/cicro/proj/label/java

# springboot项目脚本所在目录
BIN_HOME=/cicro/proj/label/bin

# springboot项目日志所在目录
LOG_HOME=/cicro/proj/label/log

# springboot打包后的jar名称
APP_NAME=label.jar

echo "构建完成，移动至部署目录"
mv -f ${WORKSPACE}/target/$APP_NAME $BASE_HOME

# 构建完成会在自动关闭进程及其子进程，造成nohub java -jar your.jar & 无效解决办法加上BUILD_ID=DONTKILLME
BUILD_ID=DONTKILLME

echo "移动成功，创建日志文件"
touch $LOG_HOME/catalina.out

echo "创建成功，执行启动脚本"
sh $BIN_HOME/labeld.sh restart