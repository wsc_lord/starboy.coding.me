# 为站点添加favicon站点图标

## 快速导航

- [```[生成ico图标文件]``` 生成图标文件](#生成图标文件)
- [```[生成svg图标文件]``` 生成svg图标文件](#生成svg图标文件)
- [```[index页面添加标签]``` index页面添加标签](#index页面添加标签)

## 生成ico图标文件

### [比特虫](http://www.bitbug.net)

> http://www.bitbug.net

访问 比特虫(http://www.bitbug.net) 官网生成favicon.ico文件并保存。

## 生成svg图标文件

### [BeJSON](http://www.bejson.com/convert/image_to_svg/)

> http://www.bejson.com/convert/image_to_svg/

访问 BeJSON(http://www.bejson.com/convert/image_to_svg/) 官网生成icon.svg文件并保存。

## index页面添加标签

在项目的 index 页面中新增 link 标签，代码如下: 

~~~html
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" sizes="any" mask="" href="icon.svg">
~~~

**`\*\*` 上述两个文件的路径必须能够正常访问到才行，建议和主页放置在同一目录**