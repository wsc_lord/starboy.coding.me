# 使用MySQL产生分表自增ID表设计

## 快速导航

- [```[自增ID表]``` 自增ID表](#自增ID表)
- [```[参考资料]``` 参考资料](#参考资料)

## 自增ID表

> MySQL 自增ID表脚本

~~~sql
DROP TABLE IF EXISTS `sys_sequence`;
CREATE TABLE `sys_sequence` (
  `id` bigint(20) unsigned NOT NULL auto_increment, 
  `biz_id` bigint(20) NULL DEFAULT NULL COMMENT '业务id',
  `table_name` varchar(64) DEFAULT NULL COMMENT '业务表名',
  PRIMARY KEY (id), 
  UNIQUE KEY biz_id (biz_id) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT = '系统自增id表';
~~~

## 参考资料

[sql格式规范工具](https://www.w3cschool.cn/tools/index?name=sql_formatter)

[(转)使用MySQL产生分表自增ID方法总结](https://www.cnblogs.com/MnCu8261/p/7930528.html)

[分布式中的分库分表之后，ID 主键如何处理？](https://www.cnblogs.com/mq0036/p/11612530.html)

[分布式ID（数据库多主模式、号段模式、雪花算法）](https://zhuanlan.zhihu.com/p/370207654)