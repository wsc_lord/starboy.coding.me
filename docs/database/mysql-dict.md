# 字典表设计

## 快速导航

- [```[字典类型表]``` 字典类型表](#字典类型表)
- [```[字典类型条目表]``` 字典类型条目表](#字典类型条目表)
- [```[参考资料]``` 参考资料](#参考资料)

## 字典类型表

> MySQL 字典类型表脚本

~~~sql
CREATE TABLE `sys_dict_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id', 
  `code` varchar(32) NULL DEFAULT NULL COMMENT '编码', 
  `status` varchar(10) NULL DEFAULT NULL COMMENT '状态', 
  `name` varchar(32) NULL DEFAULT NULL COMMENT '字典类型名称', 
  `systemFlag` char(1) COLLATE utf8_bin NOT NULL COMMENT '是否是系统字典，Y-是，N-否', 
  `sort` int(11) DEFAULT NULL COMMENT '排序', 
  `createdAt` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间', 
  `updatedAt` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间', 
  PRIMARY KEY (`id`), 
  UNIQUE uniq_idx_code (`code`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE utf8_general_ci COMMENT '字典类型表';
~~~

## 字典类型条目表

> MySQL 字典类型条目表脚本

~~~sql
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id', 
  `type_id` int(11) NOT NULL COMMENT '类型id', 
  `code` varchar(32) NULL DEFAULT NULL COMMENT '字典编码', 
  `name` varchar(32) NULL DEFAULT NULL COMMENT '字典名称', 
  `status` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT 'ENABLE' COMMENT '状态（字典）', 
  `sort` int(11) DEFAULT NULL COMMENT '序号', 
  `fixed` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'N' COMMENT '是否是固定的，Y-固定，N-不固定', 
  `createdAt` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间', 
  `updatedAt` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间', 
  PRIMARY KEY (`ID`), 
  UNIQUE uniq_idx_code (`code`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE utf8_general_ci COMMENT = '字典类型条目表'
~~~

## 参考资料

[sql格式规范工具](https://www.w3cschool.cn/tools/index?name=sql_formatter)

[数据字典表设计](https://blog.csdn.net/mynamepg/article/details/80129141)

[字典表设计](http://wewelove.github.io/fcoder/2017/07/22/dictionary-table/)

[关于mysql的数据字典设计](https://my.oschina.net/u/3627093/blog/1492762)

[关于维度信息维护和字典表的一些看法](https://yq.aliyun.com/articles/39481)

[数据库怎么设计字典表](https://segmentfault.com/q/1010000004986006)

[字典表设计](https://www.jianshu.com/p/0034802afc1f)