# Blog推荐目录

## 快速导航

- [```[生活类]``` 生活类](#生活类)
- [```[技术类]``` 技术类](#技术类)

## 生活类

> 站得高才能看得远

[《垠神》](http://www.yinwang.org/)

## 技术类

> 知其然知其所以然

[FEBS 作者 Mrbird](https://mrbird.cc/)

[程序猿DD](http://blog.didispace.com/)

[廖雪峰](https://www.liaoxuefeng.com/)

[阮一峰](http://www.ruanyifeng.com/home.html)

[纯洁的微笑](http://www.ityouknow.com/)

[晓风轻技术小站](https://xwjie.github.io/)

[YouMeek](http://www.youmeek.com/)

[芋道源码](http://www.iocoder.cn/)

[江南一点雨](http://www.javaboy.org/)