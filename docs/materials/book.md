# 书籍推荐目录

## 快速导航

- [```[励志类]``` 励志类](#励志类)
- [```[技术类]``` 技术类](#技术类)

## 励志类

> 价值观

《你要如何衡量你的人生》

## 技术类

> 后端主流开发框架文档

[Thymeleaf3.0 中文文档](https://www.bbsmax.com/A/kjdwAbYOJN/)

[Thymeleaf3.0 官方文档](https://www.thymeleaf.org/doc/tutorials/3.0/usingthymeleaf.html)

[SpringBoot 官方文档](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-devtools.html#using-boot-devtools-customizing-classload)

[Spring4 中文文档](https://sunrh.gitbooks.io/spring4-reference-chinese/content/overview_of_springframework/overview_of_springframework.html)

[Spring MVC 4.2.4.RELEASE 中文文档](https://spring-mvc.linesh.tw/)

[Spring Data JPA 参考指南 中文版](https://legacy.gitbook.com/book/ityouknow/spring-data-jpa-reference-documentation/details)

[Spring 入门](http://spring.javaboy.org/)

[MyBatis 官方文档](https://mybatis.org/mybatis-3/zh/index.html)

[MySQL 官方文档](https://www.mysqlzh.com/)

[Redis 命令参考](http://doc.redisfans.com/#)

[Docker 从入门到实践](https://yeasy.gitbooks.io/docker_practice/)

[Mars 声明式API编程(DAP) 框架](http://mars-framework.com/docs/mars-java/index.html)
