* Introduction
    * [简介](README.md)

* SpringBoot
    * [RestTemplate整合HttpClient连接池](/springboot/restTemplate.md#RestTemplate)
    * [订单业务定时取消订单简单实现](/springboot/orderCancel.md#OrderCancel)
    * [SpringBoot整合Shiro类型转换问题](/springboot/shiroConver.md#ShiroConver)
    * [RestTemplate异常报错处理](/springboot/restTemplateError.md#RestTemplate)
    * [Redis实现订单支付超时处理](/springboot/orderTimeOut.md#orderTimeOut)
    * [SpringBoot脱离SpringCloud使用Feign](/springboot/springFeign.md#Feign)
    * [File转换为MultipartFile的两种方法](/springboot/fileToMultipartFile.md#MultipartFile)
    * [SpringBoot整合Shiro后无法接收银联支付回调参数问题](/springboot/shiroUnionpay.md#ShiroUnionpay)
    * [SpringBoot打包war部署](/springboot/deployWar.md#deployWar)

* Java工具类
    * [高并发情况下获取订单号(非集群)](/javautils/codeUtil.md#CodeUtil)
    * [HttpClient工具类](/javautils/httpClientUtils.md#HttpClient)
    * [Tree结构生成工具类](/javautils/categoryTreeBuilder.md#Tree)
    * [基于LRUMap实现防重复提交工具类](/javautils/idempotentUtils.md#Submit)
    * [基于orika实现对象复制](/javautils/orika.md#orika)
    * [推荐码工具类](/javautils/referralCode.md#referralCode)

* Web前端
    * [站点添加favicon站点图标](/front/favicon.md#favicon)

- DataBase
    - <details><summary><b>MySQL</b></summary>
      <p>

        - [字典表设计](/database/mysql-dict.md)
        - [自增ID表设计](/database/mysql-sequence.md)

      </p>
    - <details><summary><b>Redis</b></summary>
      <p>

        - [基础总结](/database/redis.md)
        - [主从复制](/database/redis-master-slave.md)
        - [数据持久化](/database/redis-persistence.md)
        - [哨兵高可用](/database/redis-sentinel.md)
        - [集群模式](/database/redis-cluster.md)
        - [缓存设计](/database/redis-cache.md)
        - [应用场景](/database/redis-scene.md)
        - [面试指南](/database/redis-interview.md)

      </p>
      </details>
    - <details><summary><b>MongoDB</b></summary>
      <p>

        - [安装与部署](/database/mongodb.md)
        - [CURD 操作](/database/mongodb-curd.md)
        - [Index 索引](/database/mongodb-indexes.md)
        - [操作符](/database/mongodb-operator.md)
      
      </p>
      </details>

* DevOps
    - [阿里云CentOS安装Jenkins](/devops/jenkins.md#jenkins)
    - [阿里云CentOS安装Python](/devops/python.md#python)
    - [前后端分离Nginx配置](/devops/nginx_config.md#nginx_config)
    - [CentOS7.5安装Docker](/devops/docker.md#docker)
    - [CentOS7.5安装JDK](/devops/jdk.md#jdk)
    - [CentOS7.5安装RabbitMQ](/devops/rabbitMQ.md#rabbitMQ)
    - [CentOS7.5安装Git](/devops/git.md#git)
    - [CentOS7.5安装Maven](/devops/maven.md#maven) 
    - [CentOS7.5实现Jenkins+Gogs自动发布](/devops/jenkins+gogs.md#jenkins)
    - [CentOS7.5 Docker安装MongoDB](/devops/mongoDB.md#mongo)
    - [CentOS7.5 Docker安装MySQL](/devops/mysql.md#mysql)
    - [CentOS7.5 Docker安装Nginx](/devops/nginx.md#nginx)    
    - [JAVA jar注册成windows服务](/devops/jar.md#java)


* Liunx服务脚本
    - [Tomcat启停脚本](/liunx/tomcatd.md#tomcatd)
    - [Redis启停脚本](/liunx/redisd.md#redisd)
    - [RocketMQ启停脚本](/liunx/rocketmqd.md#rocketmqd)
    - [memcached启停脚本](/liunx/mcd.md#memcached)
    - [SpringBoot启停脚本](/liunx/springbootd.md#springboot)
    - [Trash-Cli启用回收站](/liunx/trash.md#trash)

* 工具
    - [IntelliJ IDEA 自动同步配置](/tools/ideaSettingsSync.md#IDEA)
    - [Linux 连接 Cisco AnyConnect VPN](/tools/anyconnect.md#anyconnect)

* 数据结构与算法
    - [Queue 队列](/algorithm/queue.md)

* 资料
    - [书籍推荐](/materials/book.md)
    - [Blog推荐](/materials/blog.md)
    - [文章推荐](/materials/article.md)
