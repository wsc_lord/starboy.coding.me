# RocketMQ启停脚本

## 快速导航

- [```[参数说明]``` 参数说明](#参数说明)
- [```[脚本文件]``` 脚本文件](#脚本文件)

## 参数说明

> BASE_HOME RocketMQ服务安装目录

> SERVER_PORT RocketMQ服务端口号

## 脚本文件

> 特别注意：正式环境请移除转义符\

~~~bash
#!/bin/sh

BASE_HOME=/cicro/opt/RocketMQ
SERVER_PORT=8710

log () {
    echo "========>> \$1 <<========"
}

case "\$1" in
    'start' )
        log "RocketMQ start ..."
        sh \$BASE_HOME/bin/mqnamesrv &
        sleep 3s
        sh \$BASE_HOME/bin/mqbroker -n localhost:\$SERVER_PORT -c \$BASE_HOME/conf/broker.conf &
        sleep 2s
        log "RocketMQ start ok."
        ;;
    'stop' )
        log "RocketMQ stop ..."
        sh \$BASE_HOME/bin/mqshutdown broker
        sleep 3s
        sh \$BASE_HOME/bin/mqshutdown namesrv
        sleep 2s
        log "RocketMQ stop ok."
        ;;
    'restart' )
        \$0 stop
        sleep 5s
        \$0 start
        ;;
    * )
        echo "Usage: \$0 [ start | stop | restart ]"
        exit 1
        ;;
esac

~~~