# Tomcat启停脚本

## 快速导航

- [```[参数说明]``` 参数说明](#参数说明)
- [```[脚本文件]``` 脚本文件](#脚本文件)

## 参数说明

> BASE_HOME Tomcat服务器安装目录

> SERVER_PORT Tomcat服务器HTTP/1.1端口号

> JAVA_HOME JDK安装目录

## 脚本文件

> 特别注意：正式环境请移除转义符\

~~~bash
#! /bin/sh

BASE_HOME=/cicro/quick
SERVER_PORT=8900

# set customer varibales
ulimit -n 65535
export JAVA_HOME=\$BASE_HOME/opt/jdk
export JAVA_OPTS="-server -Xms1024m -Xmx1024m -XX:PermSize=64m -XX:MaxPermSize=128m -Djava.security.egd=file:/dev/./urandom"

log () {
    echo "========>> \$1 <<========"
}

get_pid_by_port () {
    PID=\$(netstat -anp | grep \$1 | grep LISTEN | awk '{printf \$7}' | cut -d/ -f1)

    if [[ "\$PID" != "" && "\${PID:0:1}" != "-" ]] ; then
        echo \$PID
    else
        echo -1
    fi
}

kill_by_port () {
    PID=\$(get_pid_by_port \$1)

    echo "[\$1] pid: [\$PID]"

    if [ "\$PID" != "-1" ] ; then
        kill -9 \$PID
    fi
}

case "\$1" in
    'start' )
        log "quick start ..."
        \$BASE_HOME/opt/tomcat/bin/startup.sh
        log "quick start ok."
        ;;
    'stop' )
        log "quick stop ..."
        kill_by_port \$SERVER_PORT
        sleep 1s
        log "quick stop ok."
        ;;
    'restart' )
        \$0 stop
        sleep 1s
        \$0 start
        ;;
    'cc' )
        > \$BASE_HOME/opt/tomcat/logs/catalina.out
        exit 1
        ;;
    'tc' )
        tail -f \$BASE_HOME/opt/tomcat/logs/catalina.out
        exit 1
        ;;
    'vc' )
        vim \$BASE_HOME/opt/tomcat/logs/catalina.out
        exit 1
        ;;
    * )
        echo "Usage: \$0 [ start | stop | restart | cc | tc | vc ]"
        exit 1
        ;;
esac

~~~