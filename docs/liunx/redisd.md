# Redis启停脚本

## 快速导航

- [```[参数说明]``` 参数说明](#参数说明)
- [```[脚本文件]``` 脚本文件](#脚本文件)

## 参数说明

> BASE_HOME Redis服务安装目录

> SERVER_HOME Redis服务bin目录

## 脚本文件

> 特别注意：正式环境请移除转义符\

~~~bash
#! /bin/sh

BASE_HOME=/cicro/opt/redis
SERVER_HOME=\${BASE_HOME}/server/bin
LISTEN_ADDR="\$(egrep '^bind' \${BASE_HOME}/conf/redis.conf | head -1 | cut -d' ' -f2)"
LISTEN_PORT="\$(egrep '^port' \${BASE_HOME}/conf/redis.conf | head -1 | cut -d' ' -f2)"

log () {
    echo "========>> \$1 <<========"
}

get_pid_by_port () {
    PID=\$(netstat -anp | grep \$1 | grep LISTEN | awk '{printf \$7}' | cut -d/ -f1)

    if [[ "\$PID" != "" && "\${PID:0:1}" != "-" ]] ; then
        echo \$PID
    else
        echo -1
    fi
}

kill_by_port () {
    PID=\$(get_pid_by_port \$1)

    echo "[\$1] pid: [\$PID]"

    if [ "\$PID" != "-1" ] ; then
        kill -9 \$PID
    fi
}

start () {
    log "redis start ..."
    log "  --> listened \${LISTEN_ADDR}:\${LISTEN_PORT}"
    \${SERVER_HOME}/redis-server \${BASE_HOME}/conf/redis.conf
    log "redis start ok."
}

stop () {
    log "redis stop ..."
    kill_by_port \${LISTEN_PORT}
    sleep 3s
    log "redis stop ok."
}

cli () {
    \${SERVER_HOME}/redis-cli -h \${LISTEN_ADDR} -p \${LISTEN_PORT}
}


case "\$1" in
    'start' )
        start
        ;;
    'stop' )
        stop
        ;;
    'restart' )
        stop
        sleep 1s
        start
        ;;
    'cli' )
        cli
        ;;
    * )
        echo "Usage: \$0 [ start | stop | restart | cli ]"
        exit 1
        ;;
esac

~~~