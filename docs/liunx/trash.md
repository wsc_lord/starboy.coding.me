# Trash-Cli实现Linux服务器回收站功能

## 快速导航

- [```[安装Trash-Cli]``` 安装Trash-Cli](#安装Trash-Cli)
- [```[如何使用]``` 如何使用](#如何使用)
- [```[Autotrash使用方法]``` Autotrash使用方法](#Autotrash使用方法)
- [```[参考资料]``` 参考资料](#参考资料)

> Liunx实现类似于Windows回收站功能，避免rm -rf 带来的可怕后果！

## 安装Trash-Cli

> 对于 RHEL/CentOS 用户，使用 yum 命令来安装 Trash-Cli：

~~~bash
yum install -y python-setuptools.noarch
easy_install trash-cli
~~~

> 默认安装位置 /usr/bin/，查询trash提供的命令：

~~~bash
ls /usr/bin/ |grep trash
~~~

## 如何使用

>Trash-Cli 的使用不难，因为它提供了一个很简单的语法。Trash-Cli 提供了下面这些命令：

* autotrash： 自动清空回收站
* trash-put： 删除文件和目录（仅放入回收站中）
* trash-list ：列出被删除了的文件和目录
* trash-restore：从回收站中恢复文件或目录 trash.
* trash-rm：删除回收站中的文件
* trash-empty：清空回收站

## Autotrash使用方法

>autotrash 提供了下面这些命令：

* autotrash -d 30   #删除回收站中超过 30 天的文件
* autotrash --min-free 1024 -d 30  #如果回收站的可用空间少于 1GB，那么 autotrash 将从回收站中清除超过 30 天的已删除文件

## 参考资料
[使用trash-cli给Linux服务器加一个回收站的功能](http://www.eryajf.net/2535.html)

[Trash-Cli：Linux 上的命令行回收站工具](https://www.linuxidc.com/Linux/2018-09/154209.htm)

[Linux 下启用回收站](https://www.jianshu.com/p/5544f2cc562b)

[Autotrash：一个自动清除旧垃圾的命令行工具](https://linux.cn/article-10038-1.html)