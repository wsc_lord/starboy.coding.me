# memcached启停脚本

## 快速导航

- [```[参数说明]``` 参数说明](#参数说明)
- [```[脚本文件]``` 脚本文件](#脚本文件)

## 参数说明

> BASE_HOME memcached服务安装目录

> LISTEN_ADDR memcached服务地址

> LISTEN_PORT memcached服务端口

## 脚本文件

> 特别注意：正式环境请移除转义符\

```bash
#! /bin/sh

BASE_HOME=/cicro/opt/memcached
LISTEN_ADDR=192.168.1.170
LISTEN_PORT=9309

log () {
    echo "========>> \$1 <<========"
}

get_pid_by_port () {
    PID=\$(netstat -anp | grep \$1 | grep LISTEN | awk '{printf \$7}' | cut -d/ -f1)

    if [[ "\$PID" != "" && "\${PID:0:1}" != "-" ]] ; then
        echo \$PID
    else
        echo -1
    fi
}

kill_by_port () {
    PID=\$(get_pid_by_port \$1)

    echo "[\$1] pid: [\$PID]"

    if [ "\$PID" != "-1" ] ; then
        kill -9 \$PID
    fi
}

start () {
    log "mencached start ..."
    log "  --> listened \${LISTEN_ADDR}:\${LISTEN_PORT}"
    \${BASE_HOME}/bin/memcached -d -m 1024 -u root -l \${LISTEN_ADDR} -p \${LISTEN_PORT} -c 1024 -P /cicro/opt/memcached/tmp/memcached.pid #-vv
    log "mencached start ok."
}

stop () {
    log "mencached stop ..."
    kill_by_port \${LISTEN_PORT}
    sleep 3s
    log "mencached stop ok."
}

case "\$1" in
    'start' )
        start
        ;;
    'stop' )
        stop
        ;;
    'restart' )
        stop
        sleep 1s
        start
        ;;
    * )
        echo "Usage: \$0 [ start | stop | restart ]"
        exit 1
        ;;
esac

```