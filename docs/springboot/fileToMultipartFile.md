# File转换为MultipartFile的两种方法

## 快速导航

- [```[添加依赖]``` 添加依赖](#添加依赖)
- [```[MockMultipartFile实现方式]``` MockMultipartFile实现方式](#MockMultipartFile实现方式)
- [```[CommonsFileupload实现方式]``` CommonsFileupload实现方式](#CommonsFileupload实现方式)
- [```[调用示例]``` 调用示例](#调用示例)
- [```[参考资料]``` 参考资料](#参考资料)

## 添加依赖

### Maven

> 集成spring-mock（spring-test的依赖中已经包含spring-mock：版本要跟随你的spring版本定）

在项目的 pom.xml 的 dependencies 中加入以下内容:

~~~xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-mock</artifactId>
    <version>2.0.8</version>
</dependency>
~~~

## MockMultipartFile实现方式

MockMultipartFile实现方式，代码如下: 

~~~java
public static MultipartFile fileToMultipartFileByMock(File file) throws IOException {
    InputStream inputStream = new FileInputStream(file);
    MultipartFile multipartFile = new MockMultipartFile(file.getPath(), file.getName(),
            MediaType.APPLICATION_OCTET_STREAM_VALUE, inputStream);
    log.info("file转multipartFile成功. spring-mock依赖{}", multipartFile);
    return multipartFile;
}
~~~

> 集成commons-fileupload

在项目的 pom.xml 的 dependencies 中加入以下内容:

~~~xml
<dependency>
    <groupId>commons-fileupload</groupId>
    <artifactId>commons-fileupload</artifactId>
    <version>1.3.3</version>
</dependency>
~~~

## CommonsFileupload实现方式

CommonsFileupload实现方式，代码如下: 

~~~java
public static MultipartFile fileToMultipartFileByCommons(File file) throws IOException {
    FileItemFactory factory = new DiskFileItemFactory(16, null);
    FileItem item = factory.createItem(file.getName(), MediaType.TEXT_PLAIN_VALUE, true, file.getName());
    IoUtil.copy(FileUtil.getInputStream(file), item.getOutputStream());
    MultipartFile multipartFile = new CommonsMultipartFile(item);
    log.info("file转multipartFile成功. commons-fileupload依赖{}", multipartFile);
    return multipartFile;
}
~~~

## 调用示例

### TestMultipartFile

> TestMultipartFile 测试上述两种方式将File转换为MultipartFile

TestMultipartFile 代码如下: 

~~~java
package com.lord.test.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description: 测试File转MultipartFile
 * @author Lord
 * @date 2020年6月4日
 */
@Slf4j
public class TestMultipartFile {

    public static MultipartFile fileToMultipartFileByMock(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile(file.getPath(), file.getName(),
                MediaType.APPLICATION_OCTET_STREAM_VALUE, inputStream);
        log.info("file转multipartFile成功. spring-mock依赖{}", multipartFile);
        return multipartFile;
    }

    public static MultipartFile fileToMultipartFileByCommons(File file) throws IOException {
        FileItemFactory factory = new DiskFileItemFactory(16, null);
        FileItem item = factory.createItem(file.getName(), MediaType.TEXT_PLAIN_VALUE, true, file.getName());
        IoUtil.copy(FileUtil.getInputStream(file), item.getOutputStream());
        MultipartFile multipartFile = new CommonsMultipartFile(item);
        log.info("file转multipartFile成功. commons-fileupload依赖{}", multipartFile);
        return multipartFile;
    }

    public static void main(String[] args) throws Exception {
        String strUrl = "D:\\doc_f\\img\\test01.jpg";
        File file = new File(strUrl);
        fileToMultipartFileByMock(file);
        fileToMultipartFileByCommons(file);
    }
	
}
~~~

**`\*\*` 上述代码中使用hutool和lombok两个依赖包，需要自行导入**

## 参考资料

[java将File转换为MultipartFile的两种方法](https://blog.csdn.net/qq_42133100/article/details/90721812)

[史上最完整Java中将File转化为MultipartFile的方法（附阿里云腾讯云对象存储API对照）](https://blog.csdn.net/m0_37609579/article/details/100901358?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-4.nonecase&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-4.nonecase)

[File如何转换成MultipartFile](https://blog.csdn.net/weixin_39973810/article/details/90696781?utm_medium=distribute.pc_relevant.none-task-blog-baidujs-1)

[File转换为MultipartFile类型-----亲测可行](https://blog.csdn.net/loveer0/article/details/84108692)