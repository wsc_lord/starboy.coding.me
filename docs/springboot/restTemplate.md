# RestTemplate整合HttpClient连接池

## 快速导航

- [```[添加依赖]``` 添加依赖](#添加依赖)
- [```[修改配置文件]``` 修改配置文件](#修改配置文件)
- [```[添加核心配置类]``` 添加核心配置类RestTemplateConfig](#添加核心配置类)
- [```[调用示例]``` 调用示例](#调用示例)
- [```[参考资料]``` 参考资料](#参考资料)

## 添加依赖

### Maven

> httpclient连接池

在项目的 pom.xml 的 dependencies 中加入以下内容:

~~~xml
<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpclient</artifactId>
</dependency>
~~~

## 修改配置文件

在项目 resources 目录 application.yml 中加入以下内容: 

~~~yaml
spring:
  profiles:
    active: dev
  restTemplate:
    maxTotalConnect: 1000 #连接池的最大连接数，0代表不限；如果取0，需要考虑连接泄露导致系统崩溃的后果
    maxConnectPerRoute: 200
    connectTimeout: 3000
    readTimeout: 5000
    charset: UTF-8
~~~

## 添加核心配置类

在项目中新增 RestTemplateConfig 核心配置类，代码如下: 

~~~java
package com.mkeeper.config;

import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

// 必备
@Configuration
@ConfigurationProperties(prefix = "spring.restTemplate")
@ConditionalOnClass(value = { RestTemplate.class, CloseableHttpClient.class })
public class RestTemplateConfig {

    // java配置的优先级低于yml配置；如果yml配置不存在，会采用java配置
    // ####restTemplate的 java配置开始####
    private int maxTotalConnection = 500; // 连接池的最大连接数

    private int maxConnectionPerRoute = 100; // 同路由的并发数

    private int connectionTimeout = 2 * 1000; // 连接超时，默认2s

    private int readTimeout = 30 * 1000; // 读取超时，默认30s

    private String charset = "UTF-8";
    // ####restTemplate的 java配置结束####

    public void setMaxTotalConnection(int maxTotalConnection) {
        this.maxTotalConnection = maxTotalConnection;
    }

    public void setMaxConnectionPerRoute(int maxConnectionPerRoute) {
        this.maxConnectionPerRoute = maxConnectionPerRoute;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    // 创建HTTP客户端工厂
    @Bean(name = "clientHttpRequestFactory")
    public ClientHttpRequestFactory clientHttpRequestFactory() {
        return createClientHttpRequestFactory(this.connectionTimeout, this.readTimeout);
    }

    // 初始化RestTemplate,并加入spring的Bean工厂，由spring统一管理
    @Bean(name = "restTemplate")
    @ConditionalOnMissingBean(RestTemplate.class)
    public RestTemplate restTemplate(ClientHttpRequestFactory factory) {
        return createRestTemplate(factory);
    }

    private ClientHttpRequestFactory createClientHttpRequestFactory(int connectionTimeout, int readTimeout) {
        // maxTotalConnection 和 maxConnectionPerRoute 必须要配
        if (this.maxTotalConnection <= 0) {
            throw new IllegalArgumentException("invalid maxTotalConnection: " + maxTotalConnection);
        }
        if (this.maxConnectionPerRoute <= 0) {
            throw new IllegalArgumentException("invalid maxConnectionPerRoute: " + maxTotalConnection);
        }

        // 全局默认的header头配置
        List<Header> headers = new LinkedList<>();
        headers.add(new BasicHeader("Accept-Encoding", "gzip,deflate"));
        headers.add(new BasicHeader("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6"));

        // 禁用自动重试，需要重试时，请自行控制
        HttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(0, false);

        // 创建真正处理http请求的httpClient实例
        CloseableHttpClient httpClient = HttpClients.custom().setDefaultHeaders(headers).setRetryHandler(retryHandler)
                .build();

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
        factory.setConnectTimeout(connectionTimeout);
        factory.setReadTimeout(readTimeout);
        return factory;
    }

    private RestTemplate createRestTemplate(ClientHttpRequestFactory factory) {
        RestTemplate restTemplate = new RestTemplate(factory);

        // 我们采用RestTemplate内部的MessageConverter
        // 重新设置StringHttpMessageConverter字符集，解决中文乱码问题
        modifyDefaultCharset(restTemplate);

        // 设置错误处理器
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler());

        return restTemplate;
    }

    private void modifyDefaultCharset(RestTemplate restTemplate) {
        List<HttpMessageConverter<?>> converterList = restTemplate.getMessageConverters();
        HttpMessageConverter<?> converterTarget = null;
        for (HttpMessageConverter<?> item : converterList) {
            if (StringHttpMessageConverter.class == item.getClass()) {
                converterTarget = item;
                break;
            }
        }
        if (null != converterTarget) {
            converterList.remove(converterTarget);
        }
        Charset defaultCharset = Charset.forName(charset);
        converterList.add(1, new StringHttpMessageConverter(defaultCharset));
    }

}
~~~

## 调用示例

### TestController

> FileTestController 文件上传与下载

FileTestController 代码如下: 

~~~java
package com.mkeeper.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.Collections;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class FileTestController {
    
    @Resource
    private RestTemplate restTemplate;

    // post文件上传
    // 场景说明：只适合小文件（20MB以内）上传
    @RequestMapping("/postFile")
    public String testPostFileBody() {
        String filePath = "D:/config.png";

        // 通过磁盘文件上传，如果产生了临时文件，一定要记得删除，否则，临时文件越积越多，磁盘会爆
        FileSystemResource resource = new FileSystemResource(new File(filePath));

        String url = "***";// 测试的时候换成自己的配置
        String appId = "***";// 测试的时候换成自己的配置
        String secureKey = "***";// 测试的时候换成自己的配置
        String time = String.valueOf(System.currentTimeMillis());
        String pubStr = "1";
        String tempStr = String.format("app_id=%s&is_public=%s&time=%s&vframe=0%s", appId, pubStr, time, secureKey);
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("is_public", pubStr);
        form.add("vframe", "0");
        form.add("file", resource);
        form.add("app_id", appId);
        form.add("time", time);
        form.add("sign", DigestUtils.md5(tempStr));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        // headers.add("xx", "yy");//可以加入自定义的header头
        HttpEntity<MultiValueMap<String, Object>> formEntity = new HttpEntity<>(form, headers);
        String json = restTemplate.postForObject(url, formEntity, String.class);
        return json;
    }

    // 文件下载
    // 场景说明：只适合小文件（10MB以内）下载
    @RequestMapping("/downloadFile")
    public ResponseEntity testDownloadFile() throws Exception {
        String url = "https://capacity-local.chubaogao.com/capacity/template/9a9e8edbef44497ab5f7306fd6e6c6ee/f9404694aaec26af192c2c1c/9a9e8edbef44497ab5f7306fd6e6c6ee.pdf";
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);
        byte[] bytes = response.getBody();
        long contentLength = bytes != null ? bytes.length : 0;
        headers.setContentLength((int) contentLength);
        headers.setContentDispositionFormData("9a9e8edbef44497ab5f7306fd6e6c6ee.pdf",
                URLEncoder.encode("9a9e8edbef44497ab5f7306fd6e6c6ee.pdf", "UTF-8"));
        return new ResponseEntity<>(response.getBody(), headers, HttpStatus.OK);
    }

}
~~~

> GetTestController GET请求

GetTestController 代码如下: 

~~~java
package com.mkeeper.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Slf4j
@RestController
public class GetTestController {

    @Resource
    private RestTemplate restTemplate;

    //最简单的get操作
    @GetMapping("/baidu1/{key}")
    public String get1(@PathVariable String key) throws UnsupportedEncodingException {
        String encodeKey = URLEncoder.encode(key, "UTF-8");

        String url = "http://www.baidu.com/s?bdorz_come=1&ie=utf-8&f=8&rsv_bp=1&rsv_idx=1&tn=baidu&wd=" + encodeKey;

        return restTemplate.getForObject(url, String.class); //返回百度主站html
    }

    //需要自定义header头的get操作
    @GetMapping("/baidu2/{key}")
    public String get2(@PathVariable String key) throws UnsupportedEncodingException {
        HttpHeaders headers = new HttpHeaders();

        headers.set("MyHeaderKey", "MyHeaderValue");

        HttpEntity entity = new HttpEntity(headers);

        String encodeKey =URLEncoder.encode(key, "UTF-8");

        String url = "http://www.baidu.com/s?bdorz_come=1&ie=utf-8&f=8&rsv_bp=1&rsv_idx=1&tn=baidu&wd=" + encodeKey;

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        return  response.getBody(); //返回百度主站html
    }

}
~~~

> PostTestController POST请求

PostTestController 代码如下: 

~~~java
package com.mkeeper.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mkeeper.vo.ResultVo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class PostTestController {

    @Resource
    private RestTemplate restTemplate;

    // post表单演示
    @GetMapping("/postForm")
    public String testPostForm() {
        // 填写url
        String url = "";
        MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();

        // 填写表单
        form.add("name", "**");
        form.add("age", "**");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        // headers.add("xx", "yy");//可以加入自定义的header头
        HttpEntity<MultiValueMap<String, String>> formEntity = new HttpEntity<>(form, headers);
        String json = restTemplate.postForObject(url, formEntity, String.class);
        return json;
    }

    // post表单演示
    @GetMapping("/postEQ/H5")
    public String testPostEQ() {
        String url = "http://service.eqxiu.com/m/e/scene/create";
        List<String> cookieList = new ArrayList<String>();
        cookieList.add("_tracker_distinct_id_=2019051648488794");
        cookieList.add("_tracker_from_id_=");
        cookieList.add("_tracker_from_user_=");
        cookieList.add("_tracker_launch_=1");
        cookieList.add("_tracker_session_id_=3d21305f-97cf-4d3f-9708-30b3c5337dff");
        cookieList.add("_tracker_share_level_=0");
        cookieList.add("_tracker_user_id_=9dada7edb28e49c4a95686097e30e15d");
        cookieList.add("canvasId=d1529b5c2011bb33e02ca833323dc763");
        cookieList.add("JSESSIONID=cc769948af9244cb823b0544aacefc09");
        cookieList.add("pgv_pvi=489818112");
        cookieList.add("pgv_si=s7413529600");
        cookieList.add("rememberMe="
                + "TZYmQToU215ecaGAQBUWPkXq1BuJbf5FfWUHgyBm0vwJdXpKGNAUl8HSUYLIaMpjUXunaVrzieMAQJi/R2OoHlHNeIFSE/tcpumDRnO9HoLWfhbwcWwVdJp5+ylyP1QW+I03ip1NwCdC1vPyy5NZXlVK8j42O62nfLMAq128xuUXt90nZnyf6mAZYJXsKolmm2IDOdBVsBYr4szlyayIOtZqA8QpBzDGaoPsuHDu9bwlJlhQBwnSkk9k60eS37+wDWhLpgrD6eqm6sZuSrvpaI/ohxFqZUw2gbq1fKFqrjMuxb23WoFDCzpFQD1GLs1rYZC3k+Wu0wtQey67CyZgv4buzMhIhmksP2erPtL8RrbVRQMeDNMYiez4OIn3y+jgvZedmHiY0Gk5Z5OxpjqJZ7JBashdVAWSiJvTn8RRwqdyPO7sPGWyLSbbyB2UgRMod4VcpT5qUobJpizGDls5SwWKqIdVmHOpyrWtgWvEC7uCk/tFgiQZ9Nr/DOdH79NJBDdGF1XspFudR1YMcAZBVw8XRPrYyL8Zu1Yh7sITw1+QI6fqlGBnNjYuLZVaAwQ7l46NvCkbixoqSLdfRSIhuO6TMVD4PYapiAeat1Qr5Rl8hK5ivNIJKC5bEihPLTICje8W85LU7Le9L2MbARWnhSQ6mpjwRVznzZj41vwGzgU6pM/Agod0nz2JLGGTWMdgWeljRTWz7HShoXRxT8R1z64WbWwVj8tkcRUCRVUeuySsBuGrJNCVMkleW6rXJt4FhavfgGunKmim7CUv/E2O3HeB1KPa0ObPjCaL8vEnrDrg58QInVg8r8Tq3a+2tIEI4hU80V64QGYo07MEuXN7YUU295m2huQH7MnmxoXyxPJSxGeewK2aUF3PNwiAGJh+f5zglVZa6Ai/D4oIeOs6zibDZPk6Ysc/Td5jBJlf6G6vhBzs6AJRPThueJJkTSoWfDcKhSM2a82zdu6JAbMMfwHSs3kpfmfXr9sRG34TcBjxrlfhSuS3IrmJrP+la55g6fD1LfLW3/UZKHiArP83SeASffIdRiF6OxaOlOW9ZqfT1A3l4wYaXGUhosY4/M2gzKw1qEnDIboPJ+hH85IKRL+ipITm4fp1bZe3FemZ5IjwdkIswhvhJsvnmuU85OHNQb05UUTO6qrUz7lnmcCnX709uDUs1wkCbN8VMzgJvFQWIHipu+uK/ctk7ioDXdVJg6lr3WICnlpQBezP9VMX3Z/0P5pCJzrTAmNjzKO30N7F2/2+v8jKtfmM/x68VhLxqymyniujaTCchHypAc3Dk2JT62SQeHw6wJol+4ivXQQMHdj0O9wzHq8eUXVsbelHkAm2F/VbVnavrkXq7K45cg==");

        HttpHeaders headers = new HttpHeaders();

        headers.add("Accept", "application/json, text/plain, */*");
        headers.add("Accept-Encoding", "gzip, deflate");
        headers.add("Accept-Language", "zh-CN,zh;q=0.9");
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.put(HttpHeaders.COOKIE, cookieList); // 将cookie放入header
        headers.setOrigin("http://store.eqxiu.com");
        headers.add("User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3676.400 QQBrowser/10.4.3505.400");

        MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();
        form.add("name", "");
        form.add("type", "101");
        form.add("pageMode", "2");

        HttpEntity<MultiValueMap<String, String>> formEntity = new HttpEntity<>(form, headers);
        String json = restTemplate.postForObject(url, formEntity, String.class);
        return json;
    }

    // post表单演示
    @GetMapping("/postEQ/biz")
    public String testPostBiz() {
        String url = "http://lpservice.eqxiu.com/m/lp/create?bizType=1";
        List<String> cookieList = new ArrayList<String>();
        cookieList.add("_tracker_distinct_id_=2019051648488794");
        cookieList.add("_tracker_from_id_=");
        cookieList.add("_tracker_from_user_=");
        cookieList.add("_tracker_launch_=1");
        cookieList.add("_tracker_session_id_=3d21305f-97cf-4d3f-9708-30b3c5337dff");
        cookieList.add("_tracker_share_level_=0");
        cookieList.add("_tracker_user_id_=9dada7edb28e49c4a95686097e30e15d");
        cookieList.add("canvasId=d1529b5c2011bb33e02ca833323dc763");
        cookieList.add("JSESSIONID=cc769948af9244cb823b0544aacefc09");
        cookieList.add("pgv_pvi=489818112");
        cookieList.add("pgv_si=s7413529600");
        cookieList.add("rememberMe="
                + "TZYmQToU215ecaGAQBUWPkXq1BuJbf5FfWUHgyBm0vwJdXpKGNAUl8HSUYLIaMpjUXunaVrzieMAQJi/R2OoHlHNeIFSE/tcpumDRnO9HoLWfhbwcWwVdJp5+ylyP1QW+I03ip1NwCdC1vPyy5NZXlVK8j42O62nfLMAq128xuUXt90nZnyf6mAZYJXsKolmm2IDOdBVsBYr4szlyayIOtZqA8QpBzDGaoPsuHDu9bwlJlhQBwnSkk9k60eS37+wDWhLpgrD6eqm6sZuSrvpaI/ohxFqZUw2gbq1fKFqrjMuxb23WoFDCzpFQD1GLs1rYZC3k+Wu0wtQey67CyZgv4buzMhIhmksP2erPtL8RrbVRQMeDNMYiez4OIn3y+jgvZedmHiY0Gk5Z5OxpjqJZ7JBashdVAWSiJvTn8RRwqdyPO7sPGWyLSbbyB2UgRMod4VcpT5qUobJpizGDls5SwWKqIdVmHOpyrWtgWvEC7uCk/tFgiQZ9Nr/DOdH79NJBDdGF1XspFudR1YMcAZBVw8XRPrYyL8Zu1Yh7sITw1+QI6fqlGBnNjYuLZVaAwQ7l46NvCkbixoqSLdfRSIhuO6TMVD4PYapiAeat1Qr5Rl8hK5ivNIJKC5bEihPLTICje8W85LU7Le9L2MbARWnhSQ6mpjwRVznzZj41vwGzgU6pM/Agod0nz2JLGGTWMdgWeljRTWz7HShoXRxT8R1z64WbWwVj8tkcRUCRVUeuySsBuGrJNCVMkleW6rXJt4FhavfgGunKmim7CUv/E2O3HeB1KPa0ObPjCaL8vEnrDrg58QInVg8r8Tq3a+2tIEI4hU80V64QGYo07MEuXN7YUU295m2huQH7MnmxoXyxPJSxGeewK2aUF3PNwiAGJh+f5zglVZa6Ai/D4oIeOs6zibDZPk6Ysc/Td5jBJlf6G6vhBzs6AJRPThueJJkTSoWfDcKhSM2a82zdu6JAbMMfwHSs3kpfmfXr9sRG34TcBjxrlfhSuS3IrmJrP+la55g6fD1LfLW3/UZKHiArP83SeASffIdRiF6OxaOlOW9ZqfT1A3l4wYaXGUhosY4/M2gzKw1qEnDIboPJ+hH85IKRL+ipITm4fp1bZe3FemZ5IjwdkIswhvhJsvnmuU85OHNQb05UUTO6qrUz7lnmcCnX709uDUs1wkCbN8VMzgJvFQWIHipu+uK/ctk7ioDXdVJg6lr3WICnlpQBezP9VMX3Z/0P5pCJzrTAmNjzKO30N7F2/2+v8jKtfmM/x68VhLxqymyniujaTCchHypAc3Dk2JT62SQeHw6wJol+4ivXQQMHdj0O9wzHq8eUXVsbelHkAm2F/VbVnavrkXq7K45cg==");

        HttpHeaders headers = new HttpHeaders();

        headers.add("Accept", "application/json, text/plain, */*");
        headers.add("Accept-Encoding", "gzip, deflate");
        headers.add("Accept-Language", "zh-CN,zh;q=0.9");
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.put(HttpHeaders.COOKIE, cookieList); // 将cookie放入header
        headers.setOrigin("http://store.eqxiu.com");
        headers.add("User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3676.400 QQBrowser/10.4.3505.400");

        MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();
        form.add("title", "longPage");

        HttpEntity<MultiValueMap<String, String>> formEntity = new HttpEntity<>(form, headers);
        String json = restTemplate.postForObject(url, formEntity, String.class);
        return json;
    }

    @RequestMapping("/postBody")
    public String testPostBody() {
        // 填写url
        String url = "";

        // 填写json串
        String jsonBody = "{\n    \"name\": \"XX\",\n    \"age\": \"12\",\n    \"sex\": \"man\"\n" + "}\n";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // headers.add("xx", "yy");//可以加入自定义的header头
        HttpEntity<String> bodyEntity = new HttpEntity<>(jsonBody, headers);

        // 1.直接拿原始json串
        String json = restTemplate.postForObject(url, bodyEntity, String.class);

        // 2.将原始的json传转成java对象，rest template可以自动完成
        ResultVo resultVo = restTemplate.postForObject(url, bodyEntity, ResultVo.class);
        if (resultVo != null && resultVo.success()) {
            Object res = resultVo.getData();
            log.info("处理成功，返回数据: {}", resultVo.getData());
        } else {
            log.info("处理失败，响应结果: {}", resultVo);
        }

        return json;// 返回的是分包api的json
    }

}
~~~

> PostTestController 里面使用到的ResultVo对象

ResultVo 代码如下: 

~~~java
package com.mkeeper.vo;

public class ResultVo {
    private Integer code;
    private String data;

    public boolean success(){
        return 0 == code;
    }

    public String getData(){
        return data;
    }
}
~~~

## 参考资料

[Mkeeper6](https://github.com/Mkeeper6/SpringBoot-About)
