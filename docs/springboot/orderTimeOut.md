# Redis实现订单支付超时处理

## 快速导航

- [```[添加依赖]``` 添加依赖](#添加依赖)
- [```[修改配置文件]``` 修改配置文件](#修改配置文件)
- [```[添加核心配置类]``` 添加核心配置类](#添加核心配置类)
- [```[添加过期监听器]``` 添加过期监听器](#添加过期监听器)
- [```[调用示例]``` 调用示例](#调用示例)
- [```[参考资料]``` 参考资料](#参考资料)

## 添加依赖

### Maven

> 集成redis

在项目的 pom.xml 的 dependencies 中加入以下内容:

~~~xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
~~~

## 修改配置文件

在项目 resources 目录 application.yml 中加入以下内容: 

~~~yaml
spring:
  redis:
    host: 127.0.0.1
    port: 6379
    password: 
    database: 0
~~~

## 添加核心配置类

在项目中新增 RedisListenerConfig 核心配置类（redis监听器容器配置类），代码如下: 

~~~java
package com.lord.rts.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

/**
 * redis监听器容器配置类
 * 
 * @author Lord
 */
@Configuration
public class RedisListenerConfig {
	
    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        return container;
    }
    
}
~~~

## 添加过期监听器

在项目中新增 RedisKeyExpirationListener 监听器（key 过期监听器），代码如下: 

~~~java
package com.lord.rts.listener;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * key 过期监听器
 * 
 * @author Lord
 */
@Component
@Slf4j
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 注意：我们只能获取到失效的 key 而获取不到 value，
        // 所以在设计 key 的时候可以考虑把 value 一并放到 key，然后根据规则取出 value
        String key = new String(message.getBody());
        log.info("key:{}", key);
        // message.toString()也可以获取失效的key
        String expiredKey = message.toString();
        log.info("expiredKey:{}", expiredKey);
        //如果是 order- 开头的key，将订单状态设置为 已取消
        if(expiredKey.startsWith("order-")){
            log.info("订单已过期：查询数据库对应记录是否已支付");
            // TODO
        }
    }
}
~~~

## 调用示例

### WebController

> WebController 模拟下单并将订单编号存至redis

WebController 代码如下: 

~~~java
package com.lord.rts.controller;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 编写接口测试
 * 
 * @author Lord
 */
@RestController
public class WebController {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping("/order")
    public String order() {
        // 订单号
        String orderNo = UUID.randomUUID().toString().replaceAll("-", "");
        // 假设 redis key 设置过期时间：30s
        redisTemplate.opsForValue().set(String.format("order-%s", orderNo),
                orderNo, 30, TimeUnit.SECONDS);
        return orderNo;
    }
}
~~~

## 参考资料

[基于Spring Boot实现Redis Key过期失效处理](https://www.jianshu.com/p/88bdd7d0e31d)