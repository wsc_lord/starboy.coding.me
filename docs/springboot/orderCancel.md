# 订单业务定时取消订单简单实现

## 快速导航

- [```[SQL实现]]``` SQL实现](#SQL实现)
- [```[定时器实现]``` 定时器实现](#定时器实现)

## SQL实现

### MySQL

> orderStatus 订单状态（0：已支付，1：未支付，2：已取消）

取消超过15分钟未支付的订单，SQL如下:

~~~sql
<update id="updateOrderStatusByTask">
    UPDATE order_info set orderStatus = 2 where createTime &lt; CURRENT_TIMESTAMP - INTERVAL 15 MINUTE and orderStatus = 1
</update>
~~~

## 定时器实现

### Scheduled

> 取消长时间未支付的订单，超过15分钟未支付更改为取消， 每1分钟执行一次

~~~java
    /**
     * 取消长时间未支付的订单 超过15分钟未支付更改为取消
     * 每1分钟执行一次
     */
    @Scheduled(cron = "0 0/1 * * * ? ")
    @Transactional(rollbackFor = Exception.class)
    public void cancelLongTermNoPay() {
        orderService.cancelLongTermNoPay();
    }
~~~
