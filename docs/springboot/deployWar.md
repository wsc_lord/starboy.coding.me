# SpringBoot打包war部署

## 快速导航

- [```[更改项目的打包类型]``` 项目的打包类型](#项目的打包类型)
- [```[更改内置Tomcat依赖范围]``` 更改内置Tomcat依赖范围](#更改内置Tomcat依赖范围)
- [```[配置打包后的包名]``` 配置打包后的包名](#配置打包后的包名)
- [```[添加servlet初始化组件]``` 添加servlet初始化组件](#添加servlet初始化组件)
- [```[最终示例]``` 最终示例](#最终示例)

## 更改项目的打包类型

### Maven

> SpringBoot 项目的打包类型：pom、jar、war，packing默认是jar类型

部署到外部容器运行，我们需要先修改项目的 pom.xml 的以下内容:

~~~xml
<packaging>war</packaging>
~~~

## 更改内置Tomcat依赖范围

> SpringBoot 项目默认已经内置了 Tomcat 容器

因为SpringBoot内置了Tomcat，这里我们修改内置容器的依赖范围: 

~~~xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-tomcat</artifactId>
    <scope>provided</scope>
</dependency>
~~~

## 配置打包后的包名

> 默认打包后的包名为${project.artifactId}-${project.version}

因为默认的包名比较长且会出现-SNAPAHOT的字样，这里我们修改包名和项目名一致：

~~~xml
<build>
    <finalName>${project.artifactId}</finalName>
</build>
~~~

## 添加servlet初始化组件

> 部署到外部Tomcat需要添加servlet初始化组件

在项目中新增 Application 类的位置，新建 SpringApplicationBuilder 类，代码如下: 

~~~java
package com.ti.kns;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}
~~~

## 最终示例

![项目war包](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200724114206.png)

![外部Tomcat部署](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200724114649.png)

![启动日志](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200724114945.png)