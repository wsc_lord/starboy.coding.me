# 前后端分离Nginx配置

## 快速导航

- [```[主文件配置]``` 主文件配置](#主文件配置)
- [```[访问文件配置]``` 访问文件配置](#访问文件配置)

## 主文件配置

> nginx.conf 核心配置文件：

~~~nginx
user root;
pid /tis/opt/nginx/logs/nginx.pid;
worker_processes auto; # 启动进程，通常设置成和cpu的数量相等，默认为1
worker_rlimit_nofile 51200;

events {
    use epoll;
    worker_connections 1024; #单个后台worker_process进程的最大并发连接数
    multi_accept on;
}

http {
    include mime.types; # 设定mime类型,类型由mime.type文件定义
    default_type application/octet-stream; # 默认文件类型，默认为text/plain
    server_names_hash_bucket_size 128; # 解决多个server配置相同端口号，无法启动的问题，值为32的倍数
    client_header_buffer_size 32k;
	large_client_header_buffers 4 32k;
    client_max_body_size 1024m; # 上传文件大小限制
    client_body_buffer_size 10m;
    sendfile on; # 对于普通应用，必须设为 on，如果用来进行下载等应用磁盘IO重负载应用，可设置为 off，以平衡磁盘与网络I/O处理速度
    tcp_nopush on;
    keepalive_timeout 65; #连接超时时间，单位：秒
    server_tokens off;
    tcp_nodelay on;
    
    underscores_in_headers on; # 允许header中的参数名包含下划线

    fastcgi_connect_timeout 300;
	fastcgi_send_timeout 300;
	fastcgi_read_timeout 300;
	fastcgi_buffer_size 128k; 
	fastcgi_buffers 8 128k; # 设置fastcgi缓冲区为8块128k大小的空间
	fastcgi_busy_buffers_size 128k;
	fastcgi_temp_file_write_size 128k;
	fastcgi_intercept_errors on;

    # logging
	access_log /tis/opt/nginx/logs/access.log;
	error_log /tis/opt/nginx/logs/error.log warn;

    # Gzip Compression
    gzip on;
	gzip_buffers 16 8k;
	gzip_comp_level 6;
	gzip_http_version 1.1;
	gzip_min_length 256;
	gzip_proxied any;
	gzip_vary on;
    gzip_types 
        text/xml application/xml application/atom+xml application/rss+xml application/xhtml+xml image/svg+xml
		text/javascript application/javascript application/x-javascript
		text/x-json application/json application/x-web-app-manifest+json
		text/css text/plain text/x-component
		font/opentype application/x-font-ttf application/vnd.ms-fontobject
		image/x-icon;
    gzip_disable "MSIE [1-6]\.(?!.*SV1)";

    # If you have a lot of static files to serve through Nginx then caching of the files' metadata (not the actual files' contents) can save some latency.
	open_file_cache max=1000 inactive=20s;
	open_file_cache_valid 30s;
	open_file_cache_min_uses 2;
	open_file_cache_errors on;
    
    ##########################vhost#############################
    include /tis/opt/nginx/conf/vhost/*.conf; #引入各项目的子配置文件 
}
~~~

## 访问文件配置

vhost/*.conf 为每个项目访问文件，每一个单独的配置文件为一个项目配置，多个项目需要修改对应的域名、安全证书目录、服务器接口地址、上传文件访问配置等等。

> vhost/kittenish.conf 为当前项目配置：

~~~nginx
server {
    listen 80;
    server_name www.kittenish.ltd;
    rewrite ^(.*) https://\$server_name\$1 permanent; #正式环境请移除转义符\
}

server {
    #配置Https 阿里云
    listen 443 ssl;
    server_name www.kittenish.ltd;

    ssl_certificate      /opt/nginx/conf/cert/3136269_www.kittenish.ltd.pem;
    ssl_certificate_key  /opt/nginx/conf/cert/3136269_www.kittenish.ltd.key;
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout 5m;
    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    
    #Vue编译文件目录
    location /{
        alias /tis/web/wechat/dist;
        index index.html index.htm;
        try_files \$uri \$uri/ /admin/index.html; #正式环境请移除转义符\
    }

    #服务器接口地址
    location /kittenish {
	    proxy_pass http://127.0.0.1:8080/kittenish; #正式环境请移除转义符\
		proxy_set_header  Host  \$http_host; 
		proxy_set_header X-Real-IP \$remote_addr;   #正式环境请移除转义符\
		proxy_set_header  X-Forwarded-For \$proxy_add_x_forwarded_for;  #正式环境请移除转义符\
	}
    
    #上传文件访问配置，/public和/static开头的文件不需要进行拦截，直接允许访问
    location /file/public {
        alias /tis/web/kittenish/file/public;
    }
    location /file/static {
        alias /tis/web/kittenish/file/static;
    }
    location /static {
        alias /tis/web/kittenish/admin/static;
    }
    location /img {
	    root /opt/ftp_file/kittenish;
	}

    #项目图标文件指定
    location /favicon.ico {
        root /tis/web/wechat/dist;
    }
}
~~~