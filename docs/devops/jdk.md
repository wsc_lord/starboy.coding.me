# CentOS7.5安装JDK

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[下载Linux版本JDK依赖包]``` 下载Linux版本JDK依赖包](#下载Linux版本JDK依赖包)
- [```[安装JDK]``` 安装JDK](#安装JDK)
- [```[配置环境变量]``` 配置环境变量](#配置环境变量)
- [```[使profile生效]``` 使profile生效](#使profile生效)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## 下载Linux版本JDK依赖包

这里从华为镜像网站下载，使用如下命令：

~~~bash
wget https://mirrors.huaweicloud.com/java/jdk/8u151-b12/jdk-8u151-linux-x64.tar.gz
~~~

## 安装JDK

在/usr目录下新建java目录，命令如下：

~~~bash
cd /usr
mkdir java
~~~

将jdk-8u151-linux-x64.tar.gz解压至/usr/java目录，命令如下：

~~~bash
tar -zxvf jdk-8u151-linux-x64.tar.gz -C /usr/java
~~~

## 配置环境变量

修改/etc/profile文件，命令如下：

~~~bash
vim /etc/profile
~~~

在最后加新增如下内容：

> 特别注意：正式环境请移除转义符\

~~~
export JAVA_HOME=/usr/java/jdk1.8.0_151
export JRE_HOME=\$JAVA_HOME/jre
export CLASSPATH=.:\$JAVA_HOME/lib:\$JRE_HOME/lib
export PATH=\$JAVA_HOME/bin:\$PATH
~~~

> 如下图：

![修改profile文件](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200616175823.png)

## 使profile生效

使profile生效，命令如下：

~~~bash
source /etc/profile
~~~

验证jdk是否安装成功，命令如下：

~~~bash
java -version
~~~

> 如下图

![验证jdk是否安装成功](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200616180314.png)

**`\*\*` 注意：修改profile文件后需要立即生效，必须执行source /etc/profile命令。**

## 参考资料

[JDK国内镜像](https://www.cnblogs.com/zx-coder/p/13058152.html)

[linux服务器配置jdk环境变量](https://blog.csdn.net/panchao888888/article/details/80989349)