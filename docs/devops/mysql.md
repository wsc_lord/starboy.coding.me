# CentOS7.5 Docker安装MySQL

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[docker命令安装]``` docker命令安装](#docker命令安装)
- [```[查看MySQL镜像]``` 查看MySQL镜像](#查看MySQL镜像)
- [```[启动docker容器]``` 启动docker容器](#启动docker容器)
- [```[配置MySQL]``` 配置MySQL](#配置MySQL)
- [```[使用Navicat连接MySQL]``` 使用Navicat连接MySQL](#使用Navicat连接MySQL)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## docker命令安装

docker安装，使用如下命令：

~~~bash
docker pull mysql:5.7
~~~

## 查看MySQL镜像

查看MySQL镜像，使用如下命令：

~~~bash
docker images
~~~

## 启动docker容器

启动容器，使用如下命令：

~~~bash
docker run \
--name mysql \
-d \
-p 3306:3306 \
--restart unless-stopped \
-v /lord/opt/mysql/config:/etc/mysql/conf.d \
-v /lord/opt/mysql/log:/var/log/mysql \
-v /lord/opt/mysql/data:/var/lib/mysql \
-v /lord/opt/mysql/mysql-files:/var/lib/mysql-files \
-e MYSQL_ROOT_PASSWORD=root \
-e TZ=Asia/Shanghai \
mysql:5.7
~~~

查看所有容器，使用如下命令：

~~~bash
docker ps -a
~~~

## 配置MySQL

进入容器内部，使用如下命令：

~~~bash
docker exec -it mysql /bin/bash
~~~

登录MySQL，使用如下命令：

~~~bash
mysql -uroot -p
~~~

查看用户开放的访问权限：

~~~bash
select user, host from mysql.user;
~~~

删除不需要的访问权限:

~~~bash
delete from mysql.user where user='root' and host='%';
~~~

修改 root 远程连接权限（172.17.0.1为docker0的ip地址）:

~~~bash
grant all privileges on *.* to 'root'@'172.17.0.1' identified by 'root' with grant option;
~~~

#修改密码:

~~~bash
ALTER USER 'root'@'172.17.0.1' IDENTIFIED BY 'yourpassword';
~~~

刷新MySQL的系统权限:

~~~bash
flush privileges;
~~~

## 使用Navicat连接MySQL

![启用SSH通道](https://gitee.com/wsc_lord/blogImage/raw/master/img/20230328100136.png)

![连接MySQL](https://gitee.com/wsc_lord/blogImage/raw/master/img/20230328100437.png)

## 参考资料

[Docker 安装 MySQL 8.0，详细步骤](https://blog.csdn.net/weixin_49343190/article/details/118511564)

[Docker 安装 Mysql 容器 (完整详细版)](https://blog.csdn.net/BThinker/article/details/123471514)