# 阿里云CentOS安装Jenkins

## 快速导航

- [```[升级yum]``` 升级yum](#升级yum)
- [```[安装jdk]``` 安装jdk](#安装jdk)
- [```[安装jenkins]``` 安装jenkins](#安装jenkins)
- [```[熟悉jenkins配置]``` 熟悉jenkins配置](#熟悉jenkins配置)
- [```[启动与配置]``` 启动与配置](#启动与配置)
- [```[配置阿里云安全组规则]``` 配置阿里云安全组规则](#配置阿里云安全组规则)

## 升级yum

使用 Xshell 登录到阿里云服务器，使用如下命令：

~~~bash
yum -y update
~~~

> yum -y update 升级所有包同时也升级软件和系统内核

## 安装jdk

检查 JDK 环境，使用如下命令：

~~~bash
java -version
~~~

系统未安装 JDK 就会显示如下提示：

> -bash: java: command not found

我们使用 YUM 来安装 JDK 环境，命令如下：

~~~bash
yum -y install java
~~~

安装完成后，检查 JDK 是否安装成功，命令如下：

~~~bash
java -version
~~~

显示如下信息，则表示 JDK 安装成功：

> openjdk version "1.8.0_222"    
> OpenJDK Runtime Environment (build 1.8.0_222-b10)  
> OpenJDK 64-Bit Server VM (build 25.222-b10, mixed mode)     

## 安装jenkins

1.拉取库的配置到本地对应文件并导入公钥,命令如下：

~~~bash
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
~~~

2.安装jenkins,命令如下：

> `-y`参数：回答全部问题为是

~~~bash
yum -y install jenkins
~~~

3.更新jenkins,命令如下：

~~~bash
yum update jenkins
~~~

## 熟悉jenkins配置

1.查看系统配置文件，命令如下：

~~~bash
cat /etc/sysconfig/jenkins | more
~~~

> 可以获得几个重要配置项目信息
> *  JENKINS_HOME="/var/lib/jenkins",存放jenkins 配置及工作文件
> *  JENKINS_PORT="8080",jenkins默认8080端口

2.配置文件夹(这个位置挺重要)

~~~bash
ls /var/lib/jenkins
~~~

有jobs、logs、plugins等文件夹及文件若干。勿乱删
这次主要看了看plugins文件夹，所有插件都在里面，如插件ssh-slaves,会有一个ssh-slaves文件夹及ssh-slaves.jpi。
当某个插件未安装成功时，会有一个以.tmp结尾的文件

3.日志

logs相关两个位置：

* /var/lib/jenkins/logs有logs，暂时看来没记录啥，以后再追。
* /var/log/jenkins/jenkins.log,记录了插件安装等日志，失败信息原因等很清晰，重要。

## 启动与配置

1.启动服务,命令如下：

~~~bash
service jenkins start
~~~

2.浏览器访问

> IP地址:8080

3.获取管理员密码,命令如下：

~~~bash
cat /var/lib/jenkins/secrets/initialAdminPassword
~~~

## 配置阿里云安全组规则

登录[阿里云服务平台](https://www.aliyun.com/),进入控制台，选择安装```Jenkins```的云服务器实例，进入安全组规则配置，添加规则，如下图所示：

![配置阿里云安全组规则.png](https://upload.cc/i1/2019/09/12/QnUAOM.png)

**`\*\*` 注意：必须先安装JDK,JDK安装成功后，再安装Jenkins，不然后出现，使用服务的启动方式启动后，访问不到的情况。**

## 参考资料

[centos7搭建jenkins小记](https://segmentfault.com/a/1190000007086764)