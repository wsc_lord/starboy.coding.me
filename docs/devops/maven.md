# CentOS7.5安装Maven

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[下载最新版本的Maven源码包并安装]``` 下载最新版本的Maven源码包并安装](#下载最新版本的Maven源码包并安装)
- [```[配置环境变量]``` 配置环境变量](#配置环境变量)
- [```[查看maven版本]``` 查看maven版本](#查看maven版本)
- [```[配置settings]``` 配置settings](#配置settings)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## 下载最新版本的Maven源码包并安装

下载最新版本的Maven源码包，使用如下命令：

~~~bash
wget https://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
~~~

解压到指定目录，使用如下命令：

~~~bash
tar -zxvf apache-maven-3.6.3-bin.tar.gz -C /usr/local/
mv /usr/local/apache-maven-3.6.3/ maven3.6
~~~

## 配置环境变量

配置环境变量，使用如下命令：

~~~bash
vim /etc/profile

export M2_HOME=/usr/local/maven3.6
export PATH=$M2_HOME/bin
~~~

保存退出后运行下面的命令使配置生效，或者重启服务器生效，使用如下命令：

~~~bash
source /etc/profile
~~~

## 查看maven版本

查看maven版本，使用如下命令：

~~~bash
mvn -v
~~~

**`\*\*` 显示Apache Maven 3.6.3就说明配置成功了**


## 配置settings

设置本地仓库地址，使用如下命令：

~~~bash
vim /usr/local/maven3.6/conf/settings.xml
~~~
~~~xml
<localRepository>/cicro/var/m2/repo</localRepository>
~~~

设置镜像源地址为阿里云：

~~~xml
<mirrors>
    <mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>    
      <mirrorOf>*</mirrorOf>
    </mirror>
</mirrors>
~~~

全局jdk配置，settings.xml：

~~~xml
<profile>
    <id>jdk1.8</id>
    <activation>
        <activeByDefault>true</activeByDefault>
        <jdk>1.8</jdk>
    </activation>
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    </properties>
</profile>
~~~

## 参考资料

[CentOS7安装maven](https://www.cnblogs.com/ningzijie/p/12832672.html)

[CentOS7安装maven](https://blog.csdn.net/xwj1992930/article/details/96314093)