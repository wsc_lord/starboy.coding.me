# 阿里云CentOS安装Python

## 快速导航

- [```[升级yum]``` 升级yum](#升级yum)
- [```[查看系统默认版本]``` 查看系统默认版本](#查看系统默认版本)
- [```[安装依赖包]``` 安装依赖包](#安装依赖包)
- [```[查看新版本]``` 查看新版本](#查看新版本)
- [```[安装Python]``` 安装Python](#安装Python)
- [```[替换原来版本]``` 替换原来版本](#替换原来版本)
- [```[修复yum]```修复yum](修复yum)

## 升级yum

使用 Xshell 登录到阿里云服务器，使用如下命令：

~~~bash
yum -y update
~~~

> yum -y update 升级所有包同时也升级软件和系统内核

## 查看系统默认版本

查看系统默认版本，使用如下命令：

~~~bash
python -V
~~~

显示如下信息：

> Python 2.7.5

## 安装依赖包

安装Python依赖,命令如下：

> `-y`参数：回答全部问题为是

~~~bash
yum install epel-release -y
~~~

## 查看新版本

查看新版本，使用如下命令：

~~~bash
yum search easy_install
~~~

## 安装Python

安装Python,命令如下：

~~~bash
yum install python34-pip
~~~

## 替换原来版本

替换原来版本,命令如下：

~~~bash
mv /usr/bin/python /usr/bin/python_old
mv /usr/bin/python3 /usr/bin/python
~~~

## 查看最新版本

查看最新版本,命令如下：

~~~bash
python -V
~~~

## 修复yum

修改后会导致yum无法使用的情况，修改yum配置文件,命令如下：

~~~bash
vim /usr/bin/yum
~~~

[第一步.png](https://images2018.cnblogs.com/blog/1310844/201807/1310844-20180710121637303-665551647.png)

将原来的 #!/use/bin/python  --> 改为：#!/use/bin/python2.7

[第二步.png](https://images2018.cnblogs.com/blog/1310844/201807/1310844-20180716142713253-57480583.png)

~~~bash
vi /usr/libexec/urlgrabber-ext-down
~~~

[第三步.png](https://images2018.cnblogs.com/blog/1310844/201807/1310844-20180716142812311-840542657.png)

保存并退出