# CentOS7.5安装RabbitMQ

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[配置主机名称和地址]``` 配置主机名称和地址](#配置主机名称和地址)
- [```[安装相关依赖]``` 安装相关依赖](#安装相关依赖)
- [```[下载RabbitMQ必须依赖包并安装]``` 下载RabbitMQ必须依赖包并安装](#下载RabbitMQ必须依赖包并安装)
- [```[修改配置文件]``` 修改配置文件](#修改配置文件)
- [```[服务启动和停止]``` 服务启动和停止](#服务启动和停止)
- [```[启用管控台插件]``` 启用管控台插件](#启用管控台插件)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## 配置主机名称和地址

配置主机名称，使用如下命令：

~~~bash
vim /etc/hostname
~~~

![配置主机名称](https://assets.baklib.com/5ae75102-e857-45df-be3e-697004b63bf0/image1592276272714.png)

配置主机地址，使用如下命令：

~~~bash
vim /etc/hosts
~~~

![配置主机地址](https://assets.baklib.com/5ae75102-e857-45df-be3e-697004b63bf0/image1592276502166.png)

## 安装相关依赖

安装相关依赖，使用如下命令：

~~~bash
yum install build-essential openssl openssl-devel unixODBC unixODBC-devel make gcc gcc-c++ kernel-devel m4 ncurses-devel tk tc xz
~~~

## 下载RabbitMQ必须依赖包

下载RabbitMQ必须依赖包，使用如下命令：

~~~bash
wget www.rabbitmq.com/releases/erlang/erlang-19.0.4-1.el7.centos.x86_64.rpm
wget http://repo.iotti.biz/CentOS/7/x86_64/socat-1.7.3.2-5.el7.lux.x86_64.rpm
wget www.rabbitmq.com/releases/rabbitmq-server/v3.6.15/rabbitmq-server-3.6.15-1.el7.noarch.rpm
wget https://mirrors.huaweicloud.com/java/jdk/8u151-b12/jdk-8u151-linux-x64.tar.gz
~~~

安装erlang-19.0.4-1.el7.centos.x86_64.rpm，使用如下命令：

~~~bash
rpm -ivh erlang-19.0.4-1.el7.centos.x86_64.rpm
~~~

安装socat-1.7.3.2-5.el7.lux.x86_64.rpm，使用如下命令：

~~~bash
rpm -ivh socat-1.7.3.2-5.el7.lux.x86_64.rpm
~~~

安装rabbitmq-server-3.6.15-1.el7.noarch.rpm，使用如下命令：

~~~bash
rpm -ivh rabbitmq-server-3.6.15-1.el7.noarch.rpm
~~~

## 修改配置文件

修改配置文件，使用如下命令：

~~~bash
vim /usr/lib/rabbitmq/lib/rabbitmq_server-3.6.15/ebin/rabbit.app
~~~

> 比如修改密码、配置等等，例如: loopback_ users中的<<"guest">>, 只保留guest

![配置主机地址](https://assets.baklib.com/5ae75102-e857-45df-be3e-697004b63bf0/image1592278903593.png)

## 服务启动和停止

服务启动和停止，使用如下命令：

~~~bash
#启动 rabbitmq-server start &
#停止 rabbitmqctl app_stop
~~~

安装lsof，用于查看端口占用情况，使用如下命令：

~~~bash
yum install lsof
~~~

验证rabbitmq是否启动成功，使用如下命令：

~~~bash
lsof -i:5672

# 执行结果如下，说明启动成功
COMMAND   PID     USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
beam    36808 rabbitmq   51u  IPv6  59148      0t0  TCP *:amqp (LISTEN)
~~~

## 启用管控台插件

启用管控台插件，使用如下命令：

~~~bash
rabbitmq-plugins enable rabbitmq_management

# 执行结果如下，说明启用成功
The following plugins have been enabled:
  amqp_client
  cowlib
  cowboy
  rabbitmq_web_dispatch
  rabbitmq_management_agent
  rabbitmq_management

Applying plugin configuration to rabbit@localhost... started 6 plugins.
~~~

访问管控台，管控台默认端口为15672：http://192.168.126.129:15672/

**`\*\*` 注意：这里可能会出现无法访问的情况，应该是防火墙没有关闭导致（CentOS 7.0默认使用的是firewall作为防火墙）。**

查看防火墙状态，使用如下命令：

~~~bash
firewall-cmd --state
# 执行结果如下，说明防火墙处于开启状态
running
~~~

停止firewall，使用如下命令：

~~~bash
systemctl stop firewalld.service
~~~

禁止firewall开机启动，使用如下命令：

~~~bash
systemctl disable firewalld.service

# 执行结果如下，说明禁止开机启动设置成功
Removed symlink /etc/systemd/system/multi-user.target.wants/firewalld.service.
Removed symlink /etc/systemd/system/dbus-org.fedoraproject.FirewallD1.service.
~~~

登录到管控台主页，账户密码为：guest/guest

![管控台界面](https://assets.baklib.com/5ae75102-e857-45df-be3e-697004b63bf0/image1592288141468.png)