# CentOS7.5 Docker安装Nginx

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[docker命令安装]``` docker命令安装](#docker命令安装)
- [```[查看Nginx镜像]``` 查看Nginx镜像](#查看Nginx镜像)
- [```[启动docker容器]``` 启动docker容器](#启动docker容器)
- [```[映射Nginx配置文件]``` 映射Nginx配置文件](#映射Nginx配置文件)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## docker命令安装

docker安装，使用如下命令：

~~~bash
docker pull nginx
~~~

## 查看Nginx镜像

查看Nginx镜像，使用如下命令：

~~~bash
docker images nginx
~~~

## 启动docker容器

启动容器，使用如下命令：

~~~bash
docker run -d --name nginx -p 80:80 \
-v /lord/opt/nginx/conf/nginx.conf:/etc/nginx/nginx.conf \
-v /lord/opt/nginx/conf/conf.d:/etc/nginx/conf.d \
-v /lord/opt/nginx/logs:/var/log/nginx \
-v /lord/opt/nginx/html:/usr/share/nginx/html \
--privileged=true \
nginx
~~~

查看所有容器，使用如下命令：

~~~bash
docker ps -a
~~~

## 映射Nginx配置文件

复制config文件，使用如下命令：

~~~bash
docker cp nginx:/etc/nginx/nginx.conf /lord/opt/nginx/nginx.conf
~~~

## 参考资料

[CentOS7下使用docker安装nginx](https://blog.csdn.net/magic_1024/article/details/103444103)

[docker安装使用nginx镜像(CentOS环境)](https://blog.csdn.net/kitfrankenstein/article/details/104090284)