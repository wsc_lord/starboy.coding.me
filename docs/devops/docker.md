# CentOS7.5安装Docker

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[禁用Selinux]``` 禁用Selinux](#禁用Selinux)
- [```[配置yum]``` 配置yum](#配置yum)
- [```[查看仓库中所有docker版本]``` 查看仓库中所有docker版本](#查看仓库中所有docker版本)
- [```[安装Docker]``` 安装Docker](#安装Docker)
- [```[使用Docker中国加速器]``` 使用Docker中国加速器](#使用Docker中国加速器)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## 禁用Selinux

修改/etc/selinux/config 文件，将SELINUX=enforcing改为SELINUX=disabled，重启机器即可，使用如下命令：

~~~bash
vim /etc/selinux/config
~~~

> 如下图：

![禁用Selinux](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200605101159.png)

## 配置yum

把yum包更新到最新，命令如下：

~~~bash
yum update
~~~

安装需要的软件包，命令如下：

~~~bash
yum install -y yum-utils device-mapper-persistent-data lvm2
~~~

设置yum源，命令如下：

~~~bash
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
~~~

## 查看仓库中所有docker版本

查看所有仓库中所有docker版本，并选择特定版本安装,命令如下：

~~~bash
yum list docker-ce --showduplicates | sort -r
~~~

> 如下图：

![仓库中所有docker版本](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200605104341.png)

## 安装Docker

安装Docker，这里选的是18.06.3.ce-3.el7，命令如下：

~~~bash
yum install docker-ce-18.06.3.ce-3.el7
~~~

启动Docker并加入开机启动，命令如下：

~~~bash
systemctl start docker
systemctl enable docker
~~~

验证是否安装成功，命令如下：

~~~bash
docker version
~~~

> 如下图

![验证是否安装成功](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200605110354.png)

## 使用Docker中国加速器

由于网络原因，我们在pull Image 的时候，从Docker Hub上下载会很慢，我们添加国内源，命令如下：

~~~bash
vim /etc/docker/daemon.json
~~~

> 如下图

![使用Docker中国加速器](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200605111029.png)

重起docker服务，命令如下：

~~~bash
systemctl restart docker
~~~

**`\*\*` 注意：为什么docker一定要关闭selinux，请查看下面文章链接。**

## 参考资料

[CentOS7.6安装Docker](https://www.jianshu.com/p/00e162bf587a)

[在CentOS 7中安装Docker](https://www.cnblogs.com/anliven/p/6202083.html)

[2019-03-24 为什么docker一定要关闭selinux](https://www.jianshu.com/p/3179adbff01b)