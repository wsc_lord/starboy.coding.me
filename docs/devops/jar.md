# JAVA jar注册成windows服务

## 快速导航

- [```[打包SpringBoot项目]``` 打包SpringBoot项目](#打包SpringBoot项目)
- [```[下载WinSW]``` 下载WinSW](#下载WinSW)
- [```[安装WinSW服务]``` 安装WinSW服务](#安装WinSW服务)
- [```[编辑WinSW配置文件]``` 编辑WinSW配置文件文件](#编辑WinSW配置文件)
- [```[注册到windows服务]``` 注册到windows服务](#注册到windows服务)
- [```[启动服务]``` 启动服务](#启动服务)
- [```[参考资料]``` 参考资料](#参考资料)

## 打包SpringBoot项目

使用 maven 打包SpringBoot项目为jar包，使用如下命令：

~~~bash
mvn clean package
~~~

![打包好的SpringBoot项目](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200718142114.png)

## 下载WinSW

> GitHub下载地址：[GitHub](https://github.com/winsw/winsw/releases/tag/v2.9.0)

> 国内镜像下载地址：[国内镜像](http://repo.jenkins-ci.org/releases/com/sun/winsw/winsw/)

## 安装WinSW服务

1、将java jar包和下载的WinSW.NET4.exe放在同一个文件夹目录下面

2、重命名WinSW.NET4.exe为MyApp.exe(这个可以任意取)，新建个MyApp.xml(这个必须和前者的exe文件名字相同)

> 命名可以参考如下图

![安装WinSW服务](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200718143236.png)

## 编辑WinSW配置文件

编辑febsShiroService.xml文件

> 这里定义的服务名最终就是注册到window服务中的服务命，启停服务都依赖这里指定的配置

**`\*\*` 注意：下面的服务ID和服务名不能和window自身的服务命发生冲突。**

~~~xml
<service>
	<!-- 服务ID -->
	<id>FebsShiro</id>
	<!-- 服务名 -->
	<name>FebsShiro</name>
	<!-- 服务描述 -->
	<description>FEBS权限系统.</description>
	
	<!-- 服务路径 -->
	<env name="febsShiro_app_home" value="D:\proj\febs\java"/>
	<!-- 运行方式 -->
	<executable>java</executable>
	<!-- 执行参数 -->
	<arguments>-Xms512m -Xmx512m -jar "D:\proj\febs\java\FEBS-Shiro.jar"</arguments>
	
	<!-- 开机启动 -->
	<startmode>Automatic</startmode>
	<!-- 日志输出路径 -->
	<logpath>D:\proj\febs\logs</logpath>
	<!-- 日志格式化 -->
	<log mode="roll-by-time">
		<pattern>yyyy-MM-dd</pattern>
	</log>
</service>
~~~

## 注册到windows服务

脚本中的服务程序位置，请按照您的实际路径替换即可。

> 这里提供一个一键注册的bat脚本,复制内容到记事本并修改为.bat后缀，用管理员权限打开，即可成功注册到windows服务。

~~~bat
::指定创建服务的程序
@set febs_service="D:\proj\febs\java\febsShiroService.exe"
 
::开始安装FebsShiro服务
%febs_service% install
 
pause
~~~

![完整的文件目录](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200718144610.png)

> 查看服务是否成功注册到windows服务列表，win + R 打开运行对话框，输入命令如下：

~~~bat
services.msc
~~~

![查看服务是否注册成功](https://gitee.com/wsc_lord/blogImage/raw/master/img/20200718145241.png)

## 启动服务

注：若服务安装成功，可在cmd（管理员身份）中对服务进行如下操作，使用如下命令：

~~~bash
启动服务：net start FebsShiro
停止服务：net stop FebsShiro
~~~

验证服务是否正常运行：在浏览器中打开网址http://127.0.0.1:项目端口号

**`\*\*` 注意：如果你的项目有项目名，请在端口后面加上项目名，如：http://127.0.0.1:8080/febs**

## 参考资料

[Windows下将JAVA jar注册成windows服务](https://my.oschina.net/u/4410077/blog/3323449)