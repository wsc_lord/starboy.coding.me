# CentOS7.5安装Git

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[yum命令安装]``` yum命令安装](#yum命令安装)
- [```[源码安装]``` 源码安装](#源码安装)
- [```[下载最新版本的git源码包并安装]``` 下载最新版本的git源码包并安装](#下载最新版本的git源码包并安装)
- [```[查看git版本]``` 查看git版本](#查看git版本)
- [```[Git使用]``` Git使用](#Git使用)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## yum命令安装

yum安装，使用如下命令：

~~~bash
yum install -y git
~~~

## 源码安装

安装前要手动安装下依赖包（可使用rpm -qa | grep wget命令查看是否有安装包），使用如下命令：

~~~bash
yum install -y wget
yum install -y gcc-c++
yum install -y zlib-devel perl-ExtUtils-MakeMaker curl-devel
~~~

## 下载最新版本的git源码包并安装

下载最新版本的git源码包，使用如下命令：

~~~bash
wget https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.9.0.tar.gz
~~~

解压，配置，安装，使用如下命令：

~~~bash
tar -zxvf git-2.9.0.tar.gz
cd git-2.9.0
./configure --prefix=/usr/local
make && make install
~~~

## 查看git版本

查看git版本，使用如下命令：

~~~bash
git --version
~~~

## Git使用

配置一个用于提交代码的用户，输入指令，使用如下命令：

~~~bash
git config --global user.name "用户名"
~~~

同时配置一个用户的邮箱，使用如下命令：

~~~bash
git config --global user.email "邮箱"
~~~

生成公钥和私钥（用于github），使用如下命令：

~~~bash
ssh-keygen -t rsa -b 4096 -C "xxxxx@xxxxx.com"	
~~~

## 参考资料

[CentOS7安装Git以及操作](https://blog.csdn.net/xwj1992930/article/details/96428998)