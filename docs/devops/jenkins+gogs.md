# CentOS7.5实现Jenkins+Gogs自动发布

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[下载最新版本的Jenkins安装包]``` 下载最新版本的Jenkins安装包](#下载最新版本的Jenkins安装包)
- [```[安装jenkins]``` 安装jenkins](#安装jenkins)
- [```[熟悉jenkins配置]``` 熟悉jenkins配置](#熟悉jenkins配置)
- [```[配置jenkins用户和端口号]``` 配置jenkins用户和端口号](#配置jenkins用户和端口号)
- [```[启动服务]``` 启动服务](#启动服务)
- [```[修改默认镜像源]``` 修改默认镜像源](#修改默认镜像源)
- [```[插件安装]``` 插件安装](#插件安装)
- [```[卸载jenkins]``` 卸载jenkins](#卸载jenkins)
- [```[jenkins快速操作]``` jenkins快速操作](#jenkins快速操作)
- [```[部署脚本]``` 部署脚本](#部署脚本)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## 下载最新版本的jenkins安装包

下载最新版本的jenkins安装包，使用如下命令：

~~~bash
wget https://mirrors.tuna.tsinghua.edu.cn/jenkins/redhat-stable/jenkins-2.277.1-1.1.noarch.rpm
~~~

## 安装jenkins

安装jenkins,命令如下：

~~~bash
rpm -ivh jenkins-2.277.1-1.1.noarch.rpm
~~~

## 熟悉jenkins配置

1.查看系统配置文件，命令如下：

~~~bash
cat /etc/sysconfig/jenkins | more
~~~

> 可以获得几个重要配置项目信息
> *  JENKINS_HOME="/var/lib/jenkins",存放jenkins 配置及工作文件
> *  JENKINS_PORT="8080",jenkins默认8080端口

2.配置文件夹(这个位置挺重要)

~~~bash
ls /var/lib/jenkins
~~~

有jobs、logs、plugins等文件夹及文件若干。勿乱删
这次主要看了看plugins文件夹，所有插件都在里面，如插件ssh-slaves,会有一个ssh-slaves文件夹及ssh-slaves.jpi。
当某个插件未安装成功时，会有一个以.tmp结尾的文件

3.日志

logs相关两个位置：

* /var/lib/jenkins/logs有logs，暂时看来没记录啥，以后再追。
* /var/log/jenkins/jenkins.log,记录了插件安装等日志，失败信息原因等很清晰，重要。

## 配置jenkins用户和端口号

1.jenkins 默认8080端口，如果和别的程序冲突，可以修改端口号，命令如下：

~~~bash
vim /etc/sysconfig/jenkins
~~~

2.找到用户，修改为如下：

JENKINS_USER="root"

3.找到修改端口号（此端口不冲突可以不修改），修改为如下：

JENKINS_PORT="8088"

4.修改Jenkins相关文件夹用户权限，命令如下：

~~~bash
chown -R root:root /var/lib/jenkins
chown -R root:root /var/cache/jenkins
chown -R root:root /var/log/jenkins
~~~

5.配置java路径并关闭对 update-center.json 的安全检查

~~~bash
vim /etc/init.d/jenkins
~~~

找到candidates位置，添加自己的java路径：

~~~
/usr/java/jdk1.8.0_151/bin/java
~~~

关闭对 update-center.json 的安全检查，在JAVA_CMD="$JENKINS_JAVA_CMD $JENKINS_JAVA_OPTIONS后面添加如下参数：

~~~
-Dhudson.model.DownloadService.noSignatureCheck=true
~~~

6.重新加载配置文件

~~~bash
systemctl daemon-reload
~~~

**`\*\*` 查找java安装目录位置，使用如下命令**

~~~bash
whereis java
~~~

**`\*\*` 查看Jenkins安装目录，使用如下命令**

~~~bash
rpm -ql jenkins
~~~

## 启动服务

1.启动服务,命令如下：

~~~bash
service jenkins start/stop/restart
~~~

2.浏览器访问

> IP地址:8088

[http://localhost:8088/](http://localhost:8088/)

3.获取管理员密码,命令如下：

~~~bash
cat /var/lib/jenkins/secrets/initialAdminPassword
~~~

## 修改默认镜像源

1.替换jenkins默认镜像源，使用如下命令：

~~~bash
vim /var/lib/jenkins/hudson.model.UpdateCenter.xml
~~~

将 url 修改为Jenkins中文社区镜像源：

~~~
https://cdn.jsdelivr.net/gh/jenkins-zh/update-center-mirror/tsinghua/current/update-center.json
~~~

## 插件安装

安装插件Gogs、Generic Webhook Trigger、Git Parameter、Locale plugin、pathignore

**`\*\*` 注意：新版本中文插件后，先修改Default Language项，为zh_US，插件全部安装完毕之后重启，再次设置为zh_CN，即可完全汉化。**

**`\*\*` 记得勾选Ignore browser preference and force this language to all users**

## 卸载jenkins

卸载jenkins，命令如下：

~~~bash
rpm -e jenkins
~~~

删除遗留文件，命令如下：

~~~bash
find / -iname jenkins | xargs -n 1000 rm -rf
~~~

## jenkins快速操作

1.关闭Jenkins

http://localhost:8080/exit 

2.重启Jenkies

http://localhost:8080/restart

3.重新加载配置信息

http://localhost:8080/reload

## 部署脚本

> 部署脚本:deploy.sh，启动脚本：labeld.sh

[部署脚本](https://gitee.com/wsc_lord/starboy.coding.me/tree/master/script)

## 参考资料

[Jenkins中文社区](https://gitee.com/jenkins-zh)

[Jenkins+Gogs搭建自动化部署平台](https://www.jianshu.com/p/14e356cf8bb4)

[Gogs+Jenkins自动化部署方案](https://www.jianshu.com/p/7fc6eafd31e7?from=timeline&isappinstalled=0)

[搭建Jenkins2.222.3](https://blog.csdn.net/mayancheng7/article/details/106319287/)

[JENKINS+GOGS自动化部署SPRINGBOOT项目](https://www.freesion.com/article/36241064030/)

[Jenkins Update](https://jenkins-update.davidz.cn/)

[微服务架构下的自动化部署，使用Jenkins来实现](https://juejin.cn/post/6844904023816929294)

[微服务架构Jenkins自动部署](https://blog.csdn.net/qq_41345773/article/details/105560395)