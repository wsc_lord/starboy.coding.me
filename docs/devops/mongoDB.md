# CentOS7.5 Docker安装MongoDB

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[docker命令安装]``` docker命令安装](#docker命令安装)
- [```[查看mongoDB镜像]``` 查看mongoDB镜像](#查看mongoDB镜像)
- [```[启动docker容器]``` 启动docker容器](#启动docker容器)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## docker命令安装

docker安装，使用如下命令：

~~~bash
docker pull mongo
~~~

## 查看mongoDB镜像

查看mongoDB镜像，使用如下命令：

~~~bash
docker images
~~~

## 启动docker容器

启动容器，使用如下命令：

~~~bash
docker run -d -p 27017:27017 --restart=always -v mongo_configdb:/data/configdb -v mongo_db:/data/db --name mongo mongo
~~~

查看所有容器，使用如下命令：

~~~bash
docker ps -a
~~~

## 参考资料

[Docker版MongoDB的安装](https://www.jianshu.com/p/2181b2e27021)