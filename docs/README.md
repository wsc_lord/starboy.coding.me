# Lord代码库

> 本文档是作者从事```Java Developer```以来的学习历程，旨在为大家提供一个开发中较详细的代码示例，侧重点更倾向于Java服务端所涉及的技术栈，如果本文能为您得到帮助，请给予支持！

**如何支持：**

- 点击右上角Star :star: 给予关注
- 分享给您身边更多的小伙伴

> **作者：** Lord，Java Developer，[简书作者](https://www.jianshu.com/u/4e6cbc66ba9c)。

## 技术栈目录

* [`SpringBoot`](/springboot/restTemplate.md)
* [`Java工具类`](/javautils/codeUtil.md)
* [`Web前端`](/front/favicon.md)
* [`DataBase`](/database/mysql-dict.md)
* [`Liunx`](/liunx/base.md)
* [`DevOps`](/devops/jenkins.md)
* [`工具`](/tools/ideaSettingsSync.md)
* [`资料`](/materials/base.md)

## 转载分享

建立本开源项目的初衷是基于个人学习与工作中对 Java 相关技术栈的总结记录，在这里也希望能帮助一些在学习 Java过程中遇到问题的小伙伴，如果您需要转载本仓库的一些文章到自己的博客，请按照以下格式注明出处，谢谢合作。

```
作者：Lord
链接：http://wsc_lord.gitee.io/starboy.coding.me
来源：Lord代码库
```

## 参与贡献

1. 如果您对本项目有任何建议或发现文中内容有误的，欢迎提交 issues 进行指正。
2. 对于文中我没有涉及到知识点，欢迎提交 PR。
3. 如果您有文章推荐请以 markdown 格式到邮箱 `1334996110@qq.com`，[中文技术文档的写作规范指南](https://github.com/ruanyf/document-style-guide)。