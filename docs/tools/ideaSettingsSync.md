# IntelliJ IDEA 自动同步配置

## 快速导航

- [```[GitHub建立仓库]``` GitHub建立仓库](#GitHub建立仓库)
- [```[生成Token]``` 生成Token](#生成Token)
- [```[IDEA设置repository]``` IDEA设置repository](#IDEA设置repository)
- [```[参考资料]``` 参考资料](#参考资料)

> 在我们想同步家里和公司的 VSCode 配置时，或者在我们重新安装了 VSCode 之后，都希望能快速恢复之前的配置，VSCode有一款 **Settings Sync** 插件可以搞定一切，那么如何让IDEA也具有同样的功能呢！

## GitHub建立仓库

操作步骤：Your repositories > new 

![GitHub建立仓库.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618185553.png)

## 生成Token

操作步骤：Settings > Developer settings > Personal access tokens > Generate new token > Generate token

> 进入设置

![Settings.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618190319.png)

> 进入开发人员设置

![Developer settings.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618185814.png)

> 进入个人访问令牌设置

![Generate new token.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618185933.png)

> 新建Token并设置如下图权限

![Generate token.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618185952.png)

> 妥善保管好该Token，设置IDEA的时候要用

![Copy Token.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618190011.png)

## IDEA设置repository

操作步骤：Configure > Settings Repository... > Overwrite Remote

> 设置repository

![Settings Repository.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618190036.png)

> URL填写刚才创建的GitHub仓库地址，注意：必须是https的，然后点击Overwrite Remote

![Remote.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618190055.png)

> 查看GitHub仓库是否已经同步成功

![ideaSettingsSync.png](https://gitee.com/wsc_lord/blogImage/raw/master/img/20210618190125.png)

## 参考资料

[idea设置自动分享同步配置之一](https://jingyan.baidu.com/article/363872ec690ccc6e4ba16fea.html)

[idea设置自动分享同步配置之二](https://jingyan.baidu.com/album/6b97984d07c2611ca2b0bf13.html?picindex=1)

[IntelliJ IDEA 共享 IDE 设置](https://juejin.im/post/5b6aa3dbe51d4519596be18e)

[VSCode配置同步](https://www.jianshu.com/p/771a1d1686d4)