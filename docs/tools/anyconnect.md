# Linux 连接 Cisco AnyConnect VPN

## 快速导航

- [```[确认系统信息]``` 确认系统信息](#确认系统信息)
- [```[解压安装]``` 源码安装](#解压安装)
- [```[编辑配置文件]``` 编辑配置文件](#编辑配置文件)
- [```[创建密码文件]``` 创建密码文件](#创建密码文件)
- [```[创建脚本日志文件]``` 创建脚本日志文件](#创建脚本日志文件)
- [```[创建启动脚本]``` 创建启动脚本](#创建启动脚本)
- [```[使用命令直连]``` 使用命令直连](#使用命令直连)
- [```[参考资料]``` 参考资料](#参考资料)

## 确认系统信息

使用 Xshell 登录到服务器，使用如下命令：

~~~bash
cat /etc/centos-release
~~~

> CentOS Linux release 7.5.1804 (Core)

## 解压安装

安装前要手动上传安装包openconnect.zip到/usr/local目录下，使用如下命令：

~~~bash
unzip openconnect.zip 
chmod -R 777 /usr/local/openconnect/
~~~

## 编辑配置文件

编辑配置文件，使用如下命令：

~~~bash
vim /usr/local/openconnect/conf/config
~~~

具体配置信息，如下图：

![配置文件](https://gitee.com/wsc_lord/blogImage/raw/master/img/20230801144116.png)

## 创建密码文件

创建密码文件/etc/vpnc/passwd，输入账号密码，使用如下命令：

~~~bash
touch /etc/vpnc/passwd
echo "PASSWORD" > /etc/vpnc/passwd 
~~~

## 创建脚本日志文件

创建anyconnect脚本日志文件，不然连接anyconnect会失败，使用如下命令：

~~~bash
touch /var/log/openconnect-script.log
~~~

## 创建启动脚本

创建启动脚本并指定配置文件，使用如下命令：

~~~bash
vim /bin/vpn_start 
~~~

输入内容如下：

~~~bash
#!/bin/sh
/usr/local/openconnect/scripts/vpn-connect  /usr/local/openconnect/conf/config
~~~

脚本赋予权限，使用如下命令：

~~~bash
chmod 755 /bin/vpn_start
~~~

执行启动脚本，进行vpn连接，使用如下命令：

~~~bash
/bin/sh /bin/vpn_start
~~~

## 使用命令直连

不使用脚本连接，也可以用命令直接连接，使用如下命令：

~~~bash
/usr/sbin/openconnect --script /etc/vpnc/vpnc-script --user wangsicong vpn-sg.corp.lord.com
~~~

## 参考资料

[Anyconnect的VPN环境部署(2)-在Linux客户机上连接AnyConnect VPN](https://cloud.tencent.com/developer/article/1965149?shareByChannel=link)