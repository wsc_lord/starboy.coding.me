# 推荐码工具类

## 快速导航

- [```[添加依赖]``` 添加依赖](#添加依赖)
- [```[具体实现]``` 具体实现](#具体实现)

## 添加依赖

> 在POM文件中添加如下依赖：版本和以示例代码保持一致

~~~xml
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-all</artifactId>
    <version>5.6.5</version>
</dependency>
~~~

## 具体实现

> 这个工具类是转载过来的，找不到原文链接了。

~~~java
package com.lord.label.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.hutool.core.lang.Snowflake;

/**
 * 进制转换工具，最大支持十进制和DICT.length()进制的转换
 * 1、将十进制的数字转换为指定进制的字符串；
 * 2、将其它进制的数字（字符串形式）转换为十进制的数字
 * @author xuliugen
 * @date 2018/04/23
 */
@Component
public class IdUtil {

    @Autowired
    private Snowflake snowflake;

    private static final String DICT = "0123456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";

    private static final int SEED = DICT.length();

    private static final int ID_MIN_LENGTH = 6;
    /**
     * 数字->字符映射
     */
    private static final char[] CHARS = DICT.toCharArray();
    /**
     * 字符->数字映射
     */
    private static final Map<Character, Integer> NUMBERS = new HashMap<>();

    static {
        int len = CHARS.length;
        for (int i = 0; i < len; i++) {
            NUMBERS.put(CHARS[i], i);
        }
    }

    /**
     * 根据从数据库中返回的记录ID生成对应的短网址编码
     * @param id (1-56.8billion)
     * @return
     */
    public static String encode(long id) {
        StringBuilder shortURL = new StringBuilder();
        while (id > 0) {
            int r = (int) (id % SEED);
            shortURL.insert(0, CHARS[r]);
            id = id / SEED;
        }
        int len = shortURL.length();
        while (len < ID_MIN_LENGTH) {
            shortURL.insert(0, CHARS[0]);
            len++;
        }
        return shortURL.toString();
    }

    /**
     * 根据获得的短网址编码解析出数据库中对应的记录ID
     * @param key 短网址 eg. RwTji8, GijT7Y等等
     * @return
     */
    public static long decode(String key) {
        char[] shorts = key.toCharArray();
        int len = shorts.length;
        long id = 0L;
        for (int i = 0; i < len; i++) {
            id = id + (long) (NUMBERS.get(shorts[i]) * Math.pow(SEED, len - i - 1));
        }
        return id;
    }

    public String nextShortId() {
        return encode(snowflake.nextId());
    }

    public static void main(String[] args) {
        System.out.println(encode(1323875245854855170L));
        System.out.println(decode("34dFtP4nQKU"));
        IdUtil idUtil = new IdUtil();
    }
}
~~~