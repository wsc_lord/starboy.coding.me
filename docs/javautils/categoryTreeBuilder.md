# Tree结构生成工具类

## 快速导航

- [```[数据结构]``` 数据结构](#数据结构)
- [```[具体实现]``` 具体实现](#具体实现)
- [```[测试用例]``` 测试用例](#测试用例)

## 数据结构

### CategoryTreeNode

> Tree结构数据类型封装

Tree结构数据类型封装，代码如下:

~~~java
package com.boot.component.tree.category;

import java.util.List;

public class CategoryTreeNode {

    private Integer id;

    private Integer parentId;

    private String name;

    private List<CategoryTreeNode> children;

    public CategoryTreeNode(Integer id, String name, Integer parentId) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
    }

    public CategoryTreeNode(Integer id, String name, CategoryTreeNode parent) {
        this.id = id;
        this.parentId = parent.getId();
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CategoryTreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryTreeNode> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "CategoryTreeNode{" + "id='" + id + '\'' + ", parentId='" + parentId + '\'' + ", name='" + name + '\''
                + ", children=" + children + '}';
    }

}
~~~

## 具体实现

### CategoryTreeBuilder

> Tree结构数据生成

Tree结构数据生成类封装，代码如下:

~~~java
package com.boot.component.tree.category;

import java.util.ArrayList;
import java.util.List;

import com.xiaoleilu.hutool.util.ObjectUtil;

public class CategoryTreeBuilder {

    /**
     * 使用递归方法建树
     * @param categoryList 传入的树节点列表
     * @return
     */
    public static List<CategoryTreeNode> buildTree(List<CategoryTreeNode> categoryList, Integer pid) {
        List<CategoryTreeNode> treeList = new ArrayList<>();
        categoryList.forEach(categoryTreeNode -> {
            if (categoryTreeNode.getParentId() == pid) {
                categoryTreeNode.setChildren(buildTree(categoryList, categoryTreeNode.getId()));
                treeList.add(categoryTreeNode);
            }
        });
        return treeList;
    }

    /**
     * 两层循环实现建树
     * @param treeNodes 传入的树节点列表
     * @return
     */
    public static List<CategoryTreeNode> bulid(List<CategoryTreeNode> treeNodes) {

        List<CategoryTreeNode> trees = new ArrayList<CategoryTreeNode>();

        for (CategoryTreeNode treeNode : treeNodes) {

            if (0 == treeNode.getParentId()) {
                trees.add(treeNode);
            }

            for (CategoryTreeNode it : treeNodes) {
                if (ObjectUtil.equal(it.getParentId(), treeNode.getId())) {
                    if (treeNode.getChildren() == null) {
                        treeNode.setChildren(new ArrayList<CategoryTreeNode>());
                    }
                    treeNode.getChildren().add(it);
                }
            }
        }
        return trees;
    }

    /**
     * 使用递归方法建树
     * @param treeNodes
     * @return
     */
    public static List<CategoryTreeNode> buildByRecursive(List<CategoryTreeNode> treeNodes) {
        List<CategoryTreeNode> trees = new ArrayList<CategoryTreeNode>();
        for (CategoryTreeNode treeNode : treeNodes) {
            if (0 == treeNode.getParentId()) {
                trees.add(findChildren(treeNode, treeNodes));
            }
        }
        return trees;
    }

    /**
     * 递归查找子节点
     * @param treeNodes
     * @return
     */
    public static CategoryTreeNode findChildren(CategoryTreeNode treeNode, List<CategoryTreeNode> treeNodes) {
        for (CategoryTreeNode it : treeNodes) {
            if (treeNode.getId().equals(it.getParentId())) {
                if (treeNode.getChildren() == null) {
                    treeNode.setChildren(new ArrayList<CategoryTreeNode>());
                }
                treeNode.getChildren().add(findChildren(it, treeNodes));
            }
        }
        return treeNode;
    }

}
~~~

## 测试用例

### TestCategoryTreeNodeUtil

> main方法测试运行结果

~~~java
package com.boot.test;

import java.util.ArrayList;
import java.util.List;

import com.boot.component.tree.category.CategoryTreeBuilder;
import com.boot.component.tree.category.CategoryTreeNode;

public class TestCategoryTreeNodeUtil {

    public static void main(String[] args) {

        CategoryTreeNode treeNode1 = new CategoryTreeNode(1, "广州", 0);
        CategoryTreeNode treeNode2 = new CategoryTreeNode(2, "深圳", 0);

        CategoryTreeNode treeNode3 = new CategoryTreeNode(3, "天河区", 1);
        CategoryTreeNode treeNode4 = new CategoryTreeNode(4, "越秀区", 1);
        CategoryTreeNode treeNode5 = new CategoryTreeNode(5, "黄埔区", 1);
        CategoryTreeNode treeNode6 = new CategoryTreeNode(6, "石牌", 2);
        CategoryTreeNode treeNode7 = new CategoryTreeNode(7, "百脑汇", 2);

        List<CategoryTreeNode> list = new ArrayList<CategoryTreeNode>();

        list.add(treeNode1);
        list.add(treeNode2);
        list.add(treeNode3);
        list.add(treeNode4);
        list.add(treeNode5);
        list.add(treeNode6);
        list.add(treeNode7);

        // List<CategoryTreeNode> trees = CategoryTreeBuilder.bulid(list);
        // List<CategoryTreeNode> trees_ = CategoryTreeBuilder.buildTree(list, null);
        List<CategoryTreeNode> trees = CategoryTreeBuilder.buildTree(list, 1);
        System.out.println(trees);
    }

}
~~~