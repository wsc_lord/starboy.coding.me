# 基于LRUMap实现防重复提交工具类（非集群）

## 快速导航

- [```[添加依赖]``` 添加依赖](#添加依赖)
- [```[编写IdempotentUtils工具类]``` 编写IdempotentUtils工具类](#编写IdempotentUtils工具类)
- [```[编写请求测试Controller]``` 编写请求测试Controller](#编写请求测试Controller)
- [```[参考资料]``` 参考资料](#参考资料)

## 添加依赖

> 在POM文件中添加如下依赖：版本和以示例代码保持一致

~~~xml
<!-- 集合工具类 apache commons collections -->
<dependency>
  <groupId>org.apache.commons</groupId>
  <artifactId>commons-collections4</artifactId>
  <version>4.4</version>
</dependency>
~~~

## 编写IdempotentUtils工具类

> LRU 是 Least Recently Used 的缩写，即最近最少使用，是一种常用的数据淘汰算法，选择最近最久未使用的数据予以淘汰。

~~~java
package com.lord.order.utils;

import org.apache.commons.collections4.map.LRUMap;

/**
 * 幂等性判断
 */
public class IdempotentUtils {

    // 根据 LRU(Least Recently Used，最近最少使用)算法淘汰数据的 Map 集合，最大容量 100 个
    private static LRUMap<String, Integer> reqCache = new LRUMap<>(100);

    /**
     * 幂等性判断
     *
     * @return
     */
    public static boolean judge(String id, Object lockClass) {
        synchronized (lockClass) {
            // 重复请求判断
            if (reqCache.containsKey(id)) {
                // 重复请求
                System.out.println("请勿重复提交！！！" + id);
                return false;
            }
            // 非重复请求，存储请求 ID
            reqCache.put(id, 1);
        }
        return true;
    }
}
~~~

## 编写请求测试Controller

> 调用代码如下：

~~~java
package com.lord.order.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController4 {
    @RequestMapping("/add")
    public String addUser(String id) {
        // 非空判断(忽略)...
        // -------------- 幂等性调用（开始） --------------
        if (!IdempotentUtils.judge(id, this.getClass())) {
            return "执行失败";
        }
        // -------------- 幂等性调用（结束） --------------
        // 业务代码...
        System.out.println("添加用户ID:" + id);
        return "执行成功！";
    }
}
~~~

## 参考资料

[防止数据重复提交的6种方法](https://www.cnblogs.com/vipstone/p/13328386.html)

[简单且高效的6种防止数据重复提交的方法，学到了真的太香了](https://www.jianshu.com/p/9684ed892911)