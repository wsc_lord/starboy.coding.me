# 高并发情况下获取订单号（非集群）

## 快速导航

- [```[具体实现]``` 具体实现](#具体实现)
- [```[参考资料]``` 参考资料](#参考资料)

## 具体实现

### Java代码

> 使用ThreadLocal以空间换时间解决SimpleDateFormat线程安全问题

高并发情况下获取订单号保证线程安全，代码如下:

~~~java
package com.lord.pay.order;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description: 高并发情况下获取订单号保证线程安全 
 * @author liuhongbing
 * @update Lord
 * @date 2019年9月17日
 */
public final class CodeUtil {

    private static final String DATE_FORMAT = "yyyyMMddHHmmss";
    private static final AtomicInteger atomicInteger = new AtomicInteger(1000000);

    /** 
     * 创建不连续的订单号
     *  
     * @param no 
     *            数据中心编号 
     * @return 唯一的、不连续订单号 
     */
    public static String getOrderNoByUUID(String no) {
        Integer uuidHashCode = UUID.randomUUID().toString().hashCode();
        if (uuidHashCode < 0) {
            uuidHashCode = uuidHashCode * (-1);
        }
        String date = getDateFormat().format(new Date());
        return no + date + uuidHashCode;
    }

    /** 
     * 获取同一秒钟 生成的订单号连续
     *  
     * @param no 
     *            数据中心编号 
     * @return 同一秒内订单连续的编号 
     */
    public static String getOrderNoByAtomic(String no) {
        atomicInteger.getAndIncrement();
        int i = atomicInteger.get();
        String date = getDateFormat().format(new Date());
        return no + date + i;
    }

    /** 
     * 使用ThreadLocal以空间换时间解决SimpleDateFormat线程安全问题。 
     * 创建一个ThreadLocal类变量，这里创建时用了一个匿名类，覆盖了initialValue方法，主要作用是创建时初始化实例
     */
    @SuppressWarnings("rawtypes")
    private static ThreadLocal threadLocal = new ThreadLocal() {
        @Override
        protected synchronized Object initialValue() {
            return new SimpleDateFormat(DATE_FORMAT);
        }
    };

    public static DateFormat getDateFormat() {
        return (DateFormat) threadLocal.get();
    }

    public static Date parse(String textDate) throws ParseException {
        return getDateFormat().parse(textDate);
    }

}
~~~

## 参考资料

[JAVA高并发情况下生成唯一订单号](https://bbs.csdn.net/topics/392309998)

[ThreadLocal解决SimpleDateFormat线程安全问题](https://www.jianshu.com/p/f51703b5b0d2)