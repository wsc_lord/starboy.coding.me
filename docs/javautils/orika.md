# 基于orika实现对象复制

## 快速导航

- [```[添加依赖]``` 添加依赖](#添加依赖)
- [```[编写OrikaConfig配置类]``` 编写OrikaConfig配置类](#编写OrikaConfig配置类)
- [```[编写功能测试类]``` 编写功能测试类](#编写功能测试类)
- [```[参考资料]``` 参考资料](#参考资料)

## 添加依赖

> 在POM文件中添加如下依赖：版本和以示例代码保持一致

~~~xml
<dependency>
    <groupId>ma.glasnost.orika</groupId>
    <artifactId>orika-core</artifactId>
    <version>1.5.4</version>
</dependency>
~~~

## 编写OrikaConfig配置类

> MapperFacade 用于dto ->entity的转换

~~~java
package com.lord.label.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * MapperFacade 用于dto ->entity的转换
 */
@Configuration
public class OrikaConfig {

    @Bean
    public MapperFactory mapperFactory() {
        return new DefaultMapperFactory.Builder().build();
    }

    @Bean
    public MapperFacade mapperFacade() {
        return mapperFactory().getMapperFacade();
    }
}
~~~

## 编写功能测试类

> 调用代码如下：

~~~java
package com.lord.label.orika;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lord.label.domain.User;
import com.lord.label.dto.UserDTO;

import ma.glasnost.orika.MapperFacade;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrikaTest {

    @Autowired
    private MapperFacade mapperFacade;

    @Test
    public void DtoToEntityTest() {
        User user = mapperFacade.map(getUserDTO(), User.class);
        System.out.println(user.toString());
    }

    private UserDTO getUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setLoginname("admin");
        userDTO.setPassword("123456");
        userDTO.setUpdatetime(new Date());
        userDTO.setUsername("管理员");
        userDTO.setRole("管理员角色");
        userDTO.setDept("系统部");
        return userDTO;
    }
}
~~~

## 参考资料

[Orika入门](https://www.jianshu.com/p/5daf68dc5758)

[Orika对象复制教程（完美笔记）](https://www.cnblogs.com/fuzongle/p/12609063.html)