# Lord代码库

> 本文档是作者从事```Java Developer```以来的学习历程，旨在为大家提供一个开发中较详细的代码示例，侧重点更倾向于Java服务端所涉及的技术栈，如果本文能为您得到帮助，请给予支持！

**如何支持：**

- 点击右上角Star :star: 给予关注
- 分享给您身边更多的小伙伴

> **作者：** Lord，Java Developer，[简书作者](https://www.jianshu.com/u/4e6cbc66ba9c)。

[```SpringBoot```](#SpringBoot) | [`Web前端`](#Web前端) | [`DataBase`](#DataBase) | [`Liunx`](#Liunx) | [`DevOps`](#DevOps) | [`工具`](#工具) | [`资料`](#资料)
 :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-:

**Lord代码库在线预览：** [http://wsc_lord.gitee.io/starboy.coding.me](http://wsc_lord.gitee.io/starboy.coding.me)

## SpringBoot
- **RestTemplate整合HttpClient连接池** [[more]](docs/springboot/restTemplate.md)

## Web前端
- **为站点添加favicon** [[more]](docs/front/favicon.md)

## DataBase
- **变量声明** [[more]](/docs/es6/readme.md)
    - ```[Variable]``` [新增let&const变量声明](/docs/es6/readme.md#新增声明变量)

## Liunx
- **模块** [[more]](/docs/nodejs/module.md)
    - ```[Module]``` [模块的分类](/docs/nodejs/module.md#模块的分类)

## DevOps
- **Node.js生产环境完整部署指南** [[more]](/docs/devops/node-deploy.md)
    - ```[Node.js]``` [用户权限管理及登陆服务器](/docs/devops/node-deploy.md#用户权限管理及登陆服务器)

## 工具
- **Git** [[more]](/docs/tools/ideaSettingsSync.md)
    - ```[IDEA]``` [IntelliJ IDEA 自动同步配置](/docs/tools/ideaSettingsSync.md)

## 资料
- **书籍推荐** [[more]](/docs/materials/book.md)
- **Blog推荐** [[more]](/docs/materials/blog.md)
- **文章推荐** [[more]](/docs/materials/article.md)

## 转载分享

建立本开源项目的初衷是基于个人学习与工作中对 Java 相关技术栈的总结记录，在这里也希望能帮助一些在学习 Java过程中遇到问题的小伙伴，如果您需要转载本仓库的一些文章到自己的博客，请按照以下格式注明出处，谢谢合作。

```
作者：Lord
链接：http://wsc_lord.gitee.io/starboy.coding.me
来源：Lord代码库
```

## 参与贡献

1. 如果您对本项目有任何建议或发现文中内容有误的，欢迎提交 issues 进行指正。
2. 对于文中我没有涉及到知识点，欢迎提交 PR。
3. 如果您有文章推荐请以 markdown 格式到邮箱 `1334996110@qq.com`，[中文技术文档的写作规范指南](https://github.com/ruanyf/document-style-guide)。

## 参考资料

- **[《 docsify》](https://docsify.js.org/#/zh-cn/deploy)**
- **[《nodejs技术栈》](https://github.com/Q-Angelo/Nodejs-Roadmap)**
- **[《docsify - 生成文档网站简单使用教程》](https://segmentfault.com/a/1190000017576714)**
- **[《如何写出优雅的开源项目文档》](https://cloud.tencent.com/developer/article/1475490)**
- **[《Github+docsify打造个人文档web工具》](https://blog.csdn.net/qq_33840251/article/details/90698519)**

<hr/>
**未完待续，持续更新中。。。**